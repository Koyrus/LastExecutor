import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersOrganizationsComponent } from './users-organizations.component';

describe('UsersOrganizationsComponent', () => {
  let component: UsersOrganizationsComponent;
  let fixture: ComponentFixture<UsersOrganizationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersOrganizationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersOrganizationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
