import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions,Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {ConfirmationService, Message} from "primeng/primeng";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-users-organizations',
  templateUrl: './users-organizations.component.html',
  styleUrls: ['./users-organizations.component.css']
})
export class UsersOrganizationsComponent implements OnInit {

  constructor(private http : Http , private route : ActivatedRoute, private router : Router,private confirmationService: ConfirmationService , private roleService : RoleService) {

  }
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public id_org : number;
  public licence_code : string;
  public name : string;
  public results : any[]=[];
  msgs: Message[] = [];
  displayDialog: boolean;

  deleteUser(user : number){
    this.confirmationService.confirm({
      message: 'Do you want to delete this record?',
      header: 'Delete Confirmation?',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_user: user,
          is_delete: true
        };
        console.log(user , "user")
        this.http.post(this.baseApiUrl+"/executor/admin/users/delete", postParams).subscribe(function (res) {
          console.log(res);

        });
        let index = this.results.indexOf(user);
        console.log(index, "index");
        this.results = this.results.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmed', detail: 'Record deleted'}];
      },
      reject: () => {
        this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  editUser(id_user :string , org : any){
    // let headers = new Headers();
    // headers.append("Accept", 'application/json');
    // headers.append('Content-Type', 'application/json' );
    // let options = new RequestOptions({ headers: headers });
    //
    // let postParams = {
    //   id_login_user : -1,
    //   id_user: id
    // };
    //
    // this.http.post(this.baseApiUrl+"/executor/admin/users/create/edit" , postParams , options )
    //   .subscribe(res =>{
    //     const thisref = this;
    //     console.log(res);
    //     // console.log(postParams , "Post params");
    //     thisref.results = res.json();
    //     localStorage.setItem("editUsers" , JSON.stringify(res));
    this.router.navigateByUrl("adminDashboard/editUserPerOrganization/" +id_user);

    // });

  }

  activatedUser(user){
    console.log(user, "user");

    let postParams = {
      id_login_user: -1,
      id_user: user.id_user,
      is_active: user.is_active
    };
    console.log(!user.is_active, "!organization.is_active");
    this.http.post(this.baseApiUrl+"/executor/admin/users/active", postParams).subscribe(function (res) {

      console.log(res);


    });
  }



  ngOnInit() {
    var thisref = this;
    this.route.params.subscribe(function(params){
      thisref.id_org = params['id'];
      thisref.licence_code = params['licence'];
      console.log(thisref.licence_code , 'thisref.licence_code')

    });

    let postParams = {
      id_login_user : -1,
      id_organization : toInt(thisref.id_org)
    }
    if(postParams.id_organization != null){
      this.http.post(this.baseApiUrl+'/executor/admin/organization/users' , postParams , this.roleService.getHeadersOption()).subscribe(function (res) {
        console.log(res.json());
        thisref.results = res.json();
      })
    } else {
      let postParams = {
        id_login_user : -1,
        id_organization : null
      };
      this.http.post(this.baseApiUrl+'/executor/admin/other/users',postParams,this.roleService.getHeadersOption()).subscribe(res=>{
        console.log(res.json());
      })
    }

  }

  createUserOrganization(){
    var thisref = this;
    if(thisref.id_org != null || thisref.id_org != undefined){
      this.router.navigateByUrl('adminDashboard/newuserperOrg/'+thisref.id_org+'/'+thisref.licence_code);
    } else {
      this.router.navigateByUrl('adminDashboard/newuserperOrg');
    }
  }

}
