import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute} from "@angular/router";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {Message} from "primeng/primeng";

@Component({
  selector: 'app-servicii-speze',
  templateUrl: './servicii-speze.component.html',
  styleUrls: ['./servicii-speze.component.css']
})
export class ServiciiSpezeComponent implements OnInit {
  public idurl : number;
  public main_url = GlobalVariable.globalApiUrl;
  public services : any[]=[];
  display: boolean = false;
  msgs: Message[] = [];
  public edit_speze : any[]=[];
  public new_speze : boolean = false;
  public categoryName : string;
  public selectedPriorityEntry;
  public selectedPaymentEntry;
  public selectedPayModeID : number;
  public idselected;
  public edit_fix_fee : boolean;
  public tax_type =[
    {name:'fixa','type':true },
    {name:'variabila','type':false},
  ]
  constructor(private http : Http , private route : ActivatedRoute) { }

   onSelectionPaymentChange(type:string){
    console.log(this.edit_speze['fix_fee'] , '-----------------------------------------');
     this.edit_speze['fix_fee'] = (type=='1');
    // this.selectedPaymentEntry = type;
    // this.selectedPayModeID = type.type;
    // this.idselected = type.selected;
  }

  static getHeadersOption(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    return options;
  }


  showDialog(speze) {
    var thisref = this;
    if(speze != null){
      thisref.edit_speze = speze;
      thisref.new_speze = false;
    }else {
      thisref.edit_speze = [];
      thisref.edit_speze['fix_fee'] = true;
      thisref.new_speze = true;
    }
    thisref.display = true;
  }


  saveSpeze(speze : any){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });

    let PostParams = {
      id_login_user:-1,
      id_category_service:toInt(thisref.idurl),
      id_order_service:thisref.edit_speze['id_order_service'],
      name: thisref.edit_speze['name'],
      fix_fee:thisref.edit_speze['fix_fee'],
      value_fee:thisref.edit_speze['value_fee'],
      unit:thisref.edit_speze['unit'],
      articol : thisref.edit_speze['articol']
  };
    console.log(PostParams, 'speze test1');

    this.http.put(this.main_url+'/executor/admin/nompay/order/service/save' , PostParams,options).subscribe(res => {
      console.log(res)

      // thisref.serviceArr = res.json();
      this.ngOnInit();

      //   if(this.new_speze){
      //     thisref.services.push(PostParams);
      //     thisref.display = false;
      //     console.log(thisref.services,'123123123123123123');
      //     this.msgs = [];
      //     this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
      //   }        thisref.display = false;
      //
      // }, error => {
      //   console.log("err");
      //   this.msgs = [];
      //   this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
      //   thisref.display = false;
      //
      // }
    });

    thisref.display = false;
  }
  dismissModal(){
    var thisref = this;
    thisref.display = false;
  }

  ngOnInit() {
    var thisref = this;
    this.route.params.subscribe(function(params) {
      thisref.idurl = params['id'];
      console.log(thisref.idurl);
    })
    let postParams = {
      id_login_user:-1,
      id_category_service:toInt(thisref.idurl)

    }
    console.log(postParams);
    this.http.post(thisref.main_url+'/executor/admin/nompay/order/service/list',postParams , ServiciiSpezeComponent.getHeadersOption()).subscribe((res=>{
      console.log(res.json(),'service');
      thisref.services = res.json();
    }))

  }

}
