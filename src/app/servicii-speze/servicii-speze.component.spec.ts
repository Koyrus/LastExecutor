import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiciiSpezeComponent } from './servicii-speze.component';

describe('ServiciiSpezeComponent', () => {
  let component: ServiciiSpezeComponent;
  let fixture: ComponentFixture<ServiciiSpezeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiciiSpezeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiciiSpezeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
