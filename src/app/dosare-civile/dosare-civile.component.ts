import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {ConfirmationService, Message, SelectItem} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-dosare-civile',
  templateUrl: './dosare-civile.component.html',
  styleUrls: ['./dosare-civile.component.css']
})
export class DosareCivileComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
  }

  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa: any[] = [];
  public msgs: Message[] = [];
  public brands: SelectItem[];

  displayDialog: boolean;

  constructor(private http: Http, private router: Router, private confirmationService: ConfirmationService, private roleService: RoleService) {
    var thisref = this;
    this.usersPa = [];

    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    this.http.post(this.baseApiUrl + "/executor/civil/case/all", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.usersPa = res.json();
    });
  }

  detaliiDosar(id: number) {
    this.router.navigateByUrl('/dash/detalii-dosar/' + id)
  }

  deleteDosar(dosar) {
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };


        this.http.post(this.baseApiUrl + "/executor/case/delete", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
        });
        let index = this.usersPa.indexOf(dosar);
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
      }
    });
  }

  createDosarCivil() {
    this.router.navigateByUrl('/dash/dosare-civileCreate');
  }

  editDosarCivil(id: number) {
    this.router.navigateByUrl('/dash/dosare-civileCreate/' + id);
  }

  ngOnInit() {
    this.brands = [];
    this.brands.push({label: 'Toate', value: null});
    this.brands.push({label: 'INTENTAT', value: 'INTENTAT'});
    this.brands.push({label: 'SUSPENDAT', value: 'SUSPENDAT'});
    this.brands.push({label: 'STRAMUTAT', value: 'STRAMUTAT'});
    this.brands.push({label: 'EXECUTAT', value: 'EXECUTAT'});
    this.brands.push({label: 'INCHEIAT', value: 'INCHEIAT'});

  }

}
