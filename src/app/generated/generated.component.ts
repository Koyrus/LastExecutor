import { Component, OnInit } from '@angular/core';
import {Http} from "@angular/http";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-generated',
  templateUrl: './generated.component.html',
  styleUrls: ['./generated.component.css']
})
export class GeneratedComponent implements OnInit {
  public cases : any;
  public name : any;
  constructor(private http : Http , private route : ActivatedRoute) { }

  ngOnInit() {
    var thisref = this;
    this.route.params.subscribe(function(params){
      thisref.cases = params['cases'];
      thisref.name = params['name'];

    });
  }

}
