import {AfterViewInit, Component, HostListener, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http} from "@angular/http";
import {GlobalVariable} from "../global";
import {SessionService} from "../login/sessionService";


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})
export class DashboardComponent implements OnInit, AfterViewInit {
  ngAfterViewInit() {
    // jQuery('body .ui-paginator .ui-paginator-pages .ui-paginator-page').css('background-color','#016185')
    jQuery('.badge1').attr('data-badge', this.clicks);
    jQuery('.ui-datatable-reflow .ui-datatable-data > tr > td').css('background-color', 'red!important');

  }


  public clicks: number = 0;
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public selected: string;
  public rolePermissions: any[] = [];
  private nameOfExecutor: string;
  public test: string = 'MYDATE';
  public dash_address: any;
  public dash_banks: any;
  public dash_contact: any;
  public mail: string;
  public dashj_work: any;

  constructor(private router: Router, private http: Http, private route: ActivatedRoute, private data: SessionService) {
  };

  ngOnInit(): void {
    this.data.timerFunction();

    let localVarRole = localStorage.getItem('permissions');
    this.rolePermissions = JSON.parse(JSON.parse(localVarRole)['_body']);
    this.nameOfExecutor = this.rolePermissions['nameuser'];
    this.clicks = this.rolePermissions['notify'];
    this.dash_address = this.rolePermissions['dash_address'];
    this.dash_banks = this.rolePermissions['dash_banks'];
    this.dash_contact = this.rolePermissions['dash_contact'];
    this.dashj_work = this.rolePermissions['dashj_work'];
    let dash_contact = JSON.parse(JSON.parse(localVarRole)['_body'])['dash_contact'];
    let mailSplitAfterSplit = dash_contact.split(',')[1];
    this.mail = mailSplitAfterSplit.split(':')[1];
    // if (this.rolePermissions['notify'] == 0) {
    //   $("button").removeClass("badge1")
    // } else if (this.rolePermissions['notify'] > 0) {
    //   $("button").addClass("badge1")
    // }
  }


  @HostListener('document:click', ['$event'])
  public documentClick(event: Event): void {
    if (event != null) {
      console.log("clicked");
      this.data.sessionTimer = 1800;
    }
  }

  checkValue(item: any) {
    this.selected = item;
  }

  isActive(item: string) {
    return this.selected === item;
  };

  LogOut() {
    localStorage.clear();
    this.router.navigateByUrl('')
  }
}
