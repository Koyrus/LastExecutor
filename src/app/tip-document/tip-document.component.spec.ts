import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipDocumentComponent } from './tip-document.component';

describe('TipDocumentComponent', () => {
  let component: TipDocumentComponent;
  let fixture: ComponentFixture<TipDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
