import {AfterViewInit, ChangeDetectorRef, Component, OnChanges, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions , Headers} from "@angular/http";
import {ConfirmationService, Message} from "primeng/primeng";
import {GlobalVariable} from "../global";
import {PermissionService} from "../permission-service";
import {GenerateDocComponent} from "../generate-doc/generate-doc.component";

@Component({
  selector: 'app-organizations',
  templateUrl: './organizations.component.html',
  styleUrls: ['./organizations.component.css']
})
export class OrganizationsComponent implements OnInit,AfterViewInit {


  ngAfterViewInit() {
    let thisref = this;
    jQuery('.ui-cell-data').css('word-break','break-all');

  }
  public baseApiUrl = GlobalVariable.globalApiUrl;

  private data: Response;
  id: null;
  public banks: any;
  msgs: Message[] = [];
  displayDialog: boolean;

  constructor(private router: Router, private http: Http, private route: ActivatedRoute, private confirmationService: ConfirmationService, private cd: ChangeDetectorRef ,private permissionService : PermissionService) {
    var thisref= this;
    this.http.get(this.baseApiUrl+"/executor/admin/organization" , {
    }).subscribe(function (res) {
      thisref.results = res.json();
    })
  }

  public results: any[] = [];
  public total_recors: number = 0;
  private organizationId: any;


  usersOrg(id : number , licence : string){
    this.router.navigateByUrl('adminDashboard/usersOrganizations/'+id +'/'+licence)
  }

  newOrganization(){
    this.router.navigateByUrl('adminDashboard/createOrganization');
  }
  editOrganization(id :number){
    this.router.navigateByUrl('adminDashboard/editOrganization/'+id);
    console.log(id)

  }


  deleteOrganization(organization) {
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header : 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: -1,
          id_organization: organization.id,
          is_delete: true
        };
        this.http.post(this.baseApiUrl+"/executor/admin/organization/delete", postParams).subscribe(function (res) {
          console.log(res);
        });
        let index = this.results.indexOf(organization);
        console.log(index, "index");
        this.results = this.results.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'info', summary: 'Confirmarea', detail: 'Succes'}];
      }
    });
  }

  activatedOrganization(organization) {
    console.log(organization, "organizations");
    let postParams = {
      id_login_user: -1,
      id_organization: organization.id,
      is_active: organization.is_active
    };
    console.log(!organization.is_active, "!organization.is_active");
    this.http.post( this.baseApiUrl+"/executor/admin/organization/active", postParams).subscribe(function (res) {

      console.log(res);


    });
  }

  ngOnInit():void {
    var thisref = this;
    // console.log(thisref.permissionService.getVideos(),'id_login_user')
    if(thisref.permissionService.id_login_user == null){
      // this.permissionService.init().subscribe(res=>{
      //   this.permissionService.id_login_user = res;
      //   console.log(thisref.permissionService.id_login_user);
      // });
      console.log('on if');
    }
  }




}
