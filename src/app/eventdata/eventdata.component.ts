import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {Eventservice} from "./eventservice";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {RoleService} from "../role-service";
import {Http} from "@angular/http";
import {getDate} from "ngx-bootstrap/bs-moment/utils/date-getters";
$('#calendar').fullCalendar({
  height: 650
});
@Component({
  selector: 'app-eventdata',
  templateUrl: './eventdata.component.html',
  styleUrls: ['./eventdata.component.css']
})

export class EventdataComponent implements OnInit , AfterViewInit {
  ngAfterViewInit(): void {

  }
  public priority : string;

  public disableDelete : boolean = true;
  events : any[]=[];
  eventData : any[]=[];
  header : any;
  event : MyEvent;
  dialogVisible : boolean = false;
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa: any[] = [];

  idGen : number = 100;
  constructor(private cd: ChangeDetectorRef , private headerService : RoleService , private http : Http){

  }

  handleEventClick(e) {
    this.disableDelete = false;
    this.event = new MyEvent();
    this.event.title = e.calEvent.title;
    let start = e.calEvent.start;
    let end = e.calEvent.end;
    if(e.view.name === 'month') {
      start.stripTime();
    }

    if(end) {
      end.stripTime();
      this.event.end = end.format();
    }

    this.event.id = e.calEvent.id;
    this.event.start = start.format();
    this.event.allDay = e.calEvent.allDay;
    this.dialogVisible = true;
  }

  ngOnInit(): void {

    let thisref = this;
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    let postId = {
      id_login_user : toInt(GlobalVariable.login_id)
    }
    this.http.post(this.baseApiUrl+'/executor/notify/all' , postId , this.headerService.getHeadersOption()).subscribe((res)=>{
      thisref.usersPa = res.json();
      for(let i = 0;i < thisref.usersPa.length ; i++){
        if(thisref.usersPa[i].isNotify){
          this.priority = '#ff0000'
        } else {
          this.priority = '#f0ad4e'
        }
        let eventArr =
          {
            "id" : thisref.usersPa[i].id,
            "title": thisref.usersPa[i].descriptionEvent,
            "start": thisref.usersPa[i].dateEvent,
            "backgroundColor" : this.priority,
            "borderColor" : this.priority,
            "textColor" : "#000000",
            "all-day" : true
          };
        thisref.events.push(eventArr);
      }
    })
  }
  saveEvent(){
    var thisref = this;
    let postData = {
      id_login_user : toInt(GlobalVariable.login_id),
      title : this.event['title'],
      date_event : this.event['start'],

    }
    this.http.post(this.baseApiUrl+'/executor/notify/save',postData , this.headerService.getHeadersOption()).subscribe((res)=>{
      console.log(res);

      if(res.status == 200){
        thisref.events.push({
          'id' : res.json()['id'],
          'title':postData.title,
          'start':postData.date_event,
          "backgroundColor" : "#f0ad4e",
          "borderColor" : "#f0ad4e",
          "textColor" : "#000000",
        })
      }
      thisref.dialogVisible = false;

    })
  }
  dismissModalW(event : any){
    var thisref = this;
    let postParams = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_notify : toInt(event.id)
    };
    // console.log(postParams);
    this.http.post(this.baseApiUrl+'/executor/notify/delete',postParams , this.headerService.getHeadersOption()).subscribe((res)=>{
      console.log(res);
      if(res.status == 200){
        // let index = this.events.indexOf(event);
        // this.events.splice(index,1);
        console.log(this.events.length);
        for(let i = 0;i < thisref.events.length;i++){
          if(thisref.events[i].id == event.id){
           let index = thisref.events.indexOf(thisref.events[i]);
            thisref.events.splice(index , 1);
            break;
          }
        }
        thisref.events.filter((val) => toInt(val.id) != toInt(event.id));
        // this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
        console.log(event);
        console.log(this.events.length);
        console.log(this.events);
        this.dialogVisible = false;
      }
      this.dialogVisible = false;

    })

  }
  handleDayClick(event) {
    this.disableDelete = true;

    this.event = new MyEvent();
    this.event.start = event.date.format();
    this.dialogVisible = true;
    console.log(event,'12113');
    this.cd.detectChanges();
  }
}
export class MyEvent {
  id: any;
  title: string;
  descr : string;
  start: string;
  end: string;
  allDay: boolean = true;
}
