import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {RequestOptions, Headers, Http} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-edit-organization',
  templateUrl: './edit-organization.component.html',
  styleUrls: ['./edit-organization.component.css']
})
export class EditOrganizationComponent implements OnInit {
  public id: string = null;
  public cities : any = [];
  public codeLK :any;
  public codeName :any;
  public codeEmail :any;
  public codeBankAccout :any;
  public codeIdno : any;
  public test : any;
  public banksOrganizations : any[]=[];
  public cityOrganizations : any[] =[];
  public results1 : any[]=[];
  public results : any[]=[];
  public fiscalCode : any;
  constructor(private router : Router , private route: ActivatedRoute , private http:Http) {
    let thisref= this;

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );


    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : -1,
      id_organization: toInt(thisref.id),
    };
    this.http.post(this.baseApiUrl+"/executor/admin/organization/create/edit" , postParams , options )
      .subscribe(res =>{
        console.log(res);
        var thisref= this;
        thisref.results1.push(res.json());
        thisref.results.push(thisref.results1[0].department);
        console.log(thisref.results[0]);
      });
  }
  public baseApiUrl = GlobalVariable.globalApiUrl;

  ngOnInit() {
    let thisref= this;
    this.test = localStorage.getItem('banks_For_CreateOrganization');
    this.route.params.subscribe(function(params){
      thisref.id = params['id'];
    });


    this.http.get(thisref.baseApiUrl+"/executor/admin/nomenclator/org/banks" , {
    }).subscribe(function (res) {
      thisref.banksOrganizations = res.json();

    })
    this.http.get(thisref.baseApiUrl +"/executor/admin/nomenclator/org/city" , {
    }).subscribe(function (res) {
      thisref.cityOrganizations = res.json();

    })

    this.dateCredentials.code=thisref.results['licence_code'];
    this.dateCredentials.idno=thisref.results['idno'];
    this.dateCredentials.name=thisref.results['name'];
    this.dateCredentials.email=thisref.results['email'];
    this.dateCredentials.post_code=thisref.results['post_code'];
    this.dateCredentials.street = thisref.results['street'];
    this.dateCredentials.block = thisref.results['block'];
    this.dateCredentials.bank = thisref.results['bank'];
    this.dateCredentials.tel_fax = thisref.results['tel_fax'];
    this.dateCredentials.phone = thisref.results['phone'];
    this.dateCredentials.working_hours = thisref.results['working_hours'];
    this.dateCredentials.cod_fiscal = thisref.results['fiscal_code'];

  }

  dateCredentials = {
    code: this.codeLK,
    idno: this.codeIdno,
    email: this.codeEmail,
    name : this.codeName,
    city: null,
    addressa: null ,
    post_code: null ,
    bank: null ,
    id : null,
    login_user_id : null,
    street : null,
    block : null,
    tel_fax :null,
    phone : null,
    working_hours : null,
    bank_name_id : this.codeBankAccout,
    bank_accoutId : null,
    cod_fiscal : null

  };

  public editOrganization(){
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });
    let thisref= this;
    this.route.params.subscribe(function(params){
      thisref.id = params['id'];
    });
    let postParams = {
      "id": null,
      "id_login_user": -1,
      "licence_code": this.dateCredentials.code,
      "name": this.dateCredentials.name,
      "idno": this.dateCredentials.idno,
      "city": this.cities,
      "street": this.dateCredentials.street,
      "block":this.dateCredentials.block,
      "fiscal_code": this.fiscalCode,
      "bank_account": this.dateCredentials.bank_accoutId,
      "bank": this.dateCredentials.bank,
      // "other_bank_name": "my bank",
      "email": this.dateCredentials.email,
      "post_code": this.dateCredentials.post_code,
      "working_hours": this.dateCredentials.working_hours,
      "phone": this.dateCredentials.phone,
      "tel_fax" : this.dateCredentials.tel_fax
    };
    console.log(postParams);
    this.http.post(this.baseApiUrl+"/executor/admin/organization/save" , postParams , options ).subscribe(res =>{
      console.log(res);
    });
    this.router.navigateByUrl("adminDashboard/organization");
    // localStorage.clear();
  }
}
