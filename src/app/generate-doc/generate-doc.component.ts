import {AfterViewInit, Component, EventEmitter, Injectable, Input, OnInit, Output} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Http, RequestOptions,Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {escape} from "querystring";
import {GlobalService} from "../globalService";
import {DosareStatService} from "../dosare-stat/dosare-stat.service";
import {ConfirmationService} from "primeng/primeng";
import {Observable} from "rxjs/Observable";

@Component({
  selector: 'app-generate-doc',
  templateUrl: './generate-doc.component.html',
  styleUrls: ['./generate-doc.component.css']




})
export class GenerateDocComponent implements OnInit , AfterViewInit{

  private options = new RequestOptions(
    { headers: new Headers({ 'Content-Type': 'application/json' }) });

  private CkeditorConfig = {

    allowedContent: true,
    extraAllowedContent:'[id]',
    protectedSource:'/<i[^>]></i>*id/g',
    height:1173,
    // line_height : "10em",
    width : 718,
    entities_latin : false,
    removeButtons : 'Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,NewPage,DocProps,Preview,Templates,Cut,Copy,Paste,PasteText,PasteFromWord,Link,Unlink,Anchor,Flash,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,CreateDiv,About'
  }
  ckeditorContent: any;
  public id_file:number;
  public coming_time : any;
  public coming_date : any;
  public generateDosarID : any;
  public main_url = GlobalVariable.globalApiUrl;
  public case_number : string;
  public cases : any[]=[];
  selected :any[]=[];
  public doc_id : any;
  public document_cause : string;
  public show_topBar : boolean = true;
  public id_doc_type : number;
  loader : boolean;
  public person_types =[
    {namePerson : 'Cheltuieile se aplică debitorului(-ilor)',id : 'deb'},
    {namePerson : 'Cheltuieile se aplică creditorului(-ilor)' , id : 'cred'}
  ];
  public initiere = [
    {name: 'Refuz', 'id': 4, 'code': 'KOP34IO'},
    {name: 'Intentare', 'id': 5, 'code': 'SDWE23D'},
    {name: 'Preluare', 'id': 6, 'code': 'DWE32MN'},
    {name: 'Citatie', id: 7, 'code': 'SDW2C32'},
    {name: 'Continuare', id: 8, 'code': 'SDFERDF'},
    {name: '-', id: '', 'code': ''}
  ];

  public interdictii = [
    {name: 'Asigurare', 'id': '', 'code': ''} ,
    {name: 'Anulare', 'id': '', 'code': ''},
    {name: 'Cautare Transport', 'id': '', 'code': ''},
    {name: 'Dem. Neparasire', 'id': '', 'code': ''},
    {name: 'Dem. Aducere', 'id': '', 'code': ''},
    {name: 'Dem. Acte', 'id': '', 'code': ''},
  ];

  public cheltuieli = [
    {name: 'Borderou', 'id': '', 'code': ''},
    {name: 'Nota de Plata', 'id': '', 'code': ''},
    {name: 'Incasare Cheltuieli', 'id': '', 'code': ''},
    {name: '-', id: '', 'code': ''},
    {name: '-', id: '', 'code': ''},
    {name: '-', id: '', 'code': ''},
  ];

  public finisare = [
    {name: 'Incetare', 'id': '', 'code': ''},
    {name: 'Stramutare', 'id': '', 'code': ''},
    {name: 'Suspendare', 'id': '', 'code': ''},
    {name: 'Insolvalabilitate', 'id': '', 'code': ''},
    {name: '-', id: '', 'code': ''},
    {name: '-', id: '', 'code': ''}

  ];
  public deb_array : any[]=[];
  public cred_array : any[]=[];
  public itemSelected:any[]=[];
  public if_borderou : boolean = true;
  public taxe_array : any[]=[];
  public speze_array : any[]=[];
  public other_array : any[]=[];
  public test1 : string;
  public idurl : number;
  public doc_executor : string;
  public case_array : any[] = [];
  public borderouView : boolean;


  constructor(private http : Http , private router : Router ,  private route : ActivatedRoute ,private dosareService : DosareStatService,private confirmationService: ConfirmationService) {
    var thisref = this;
    jQuery('#cred').css('padding-top','1%');
    thisref.deb_array = this.dosareService.getDebitors;
    thisref.cred_array = this.dosareService.getCreditors;
  }

  borderouBackFunc(){
    this.if_borderou = true;
    this.selected = [];
  }

  backButtonFuncToDetail(){
    this.ckeditorContent = null;
    this.ckeditorContent = '';

    this.document_cause = null;
    this.document_cause = '';

    var thisref = this;
    this.router.navigateByUrl('/dash/detalii-dosar/'+thisref.idurl);
  }

  sendGeneratedDoc(){
    var thisref = this;
    thisref.loader = true;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    let postDocCase = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_doc_type : thisref.id_doc_type,
      id_case : toInt(thisref.generateDosarID),
      document_pdf : thisref.ckeditorContent

    };
    this.http.post(thisref.main_url + '/executor/case/document/generate/pdf' , postDocCase , options).subscribe(function (res) {
    thisref.id_file = res.json()['id_file'];
      thisref.loader = false;
    })
  }

  downloadPdf(){
    var a = document.createElement('a');
    a.href=this.main_url +'/executor/case/download/'+this.id_file;
    a.click();
  }
  // downloadPdfBorderou(){
  //   var a = document.createElement('a');
  //   a.href=this.main_url +'/executor/case/download/'+this.id_file;
  //   a.click();
  // }

  display: boolean = false;

  showDialog() {
    this.display = true;
  }


  public downloadEnabled : boolean = true;
///////////////////////Переделать под Observable///////////////////
  saveBorderou(){
    this.downloadEnabled = true;
    var thisref = this;
    thisref.loader = true;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({ headers: headers });

    let postDocCase = {
      id_login_user : toInt(GlobalVariable.login_id),
      pay_items : thisref.itemSelected,
      id_case : toInt(thisref.generateDosarID),
      is_creditor : false

    };
    this.http.post(thisref.main_url + '/executor/case/borderou/save' , postDocCase , options).subscribe(function (res) {
      thisref.loader = false;
      if(res.status == 200){
        window.history.go(-1);
      }
    })

  }

  backButtonFunc(){
    this.show_topBar = true;
  }
  onSelectionChangeCase(cause : any){
    this.document_cause = cause['name'];
  }

  public personTypeSelect : boolean;
  onSelectionChangePerson(type : any){
    this.personTypeSelect = type['namePerson']
  }



  isActive(item) {
    return this.selected === item;
  };



  public payment_results : any[]=[];
  sendChelt(item : any){
    var thisref = this;
    this.if_borderou = false;
    this.id_doc_type = item.id;
    this.selected = item;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    this.http.get(this.main_url+'/executor/case/payments/order/service' , options).subscribe(function (res) {
      thisref.payment_results = res.json();
    })


  }

  public totalPrice : number=0.000;

  selectedRowStyle(selected : boolean){
    let myStyles;
      if(selected){
        myStyles = {
          'border-bottom':'2.5px solid #debb5b',
          'border-top':'2.5px solid #debb5b',

        }
      } else {
        myStyles = {
          'border-bottom':'1px solid lightgrey',
          'border-top':'1px solid lightgrey'
        }
      }


      return myStyles;
  }


  checkedValue(item_taxe : any){

    if(item_taxe.selected){
      this.itemSelected.push(item_taxe);
      if(item_taxe.amount > 0){
        item_taxe.total_amount=item_taxe.value_fee * item_taxe.amount;
      }
      if(item_taxe.fix_fee){
        item_taxe.total_amount=item_taxe=item_taxe.value_fee;
      }
    }else{
      this.itemSelected.splice(this.itemSelected.indexOf(item_taxe), 1);
      item_taxe.amount=0;
    }
    this.totalPrice = this.itemSelected.reduce((sum, val) => sum + val.total_amount, 0);
  }

  csKeyUp(item_taxe : any){
    var thisref = this;
    item_taxe['total_amount'] = (item_taxe['value_fee'] * item_taxe['amount']);
    this.totalPrice = this.itemSelected.reduce((sum, val) => sum + val.total_amount, 0);
  }

  checkValue(item : any){
    var thisref = this;
    this.id_doc_type = item.id;
    this.selected = item;

    thisref.doc_id = item;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postDocCase = {
      id_cause:item.id,
      code_cause:item.code
    };

    this.http.post(thisref.main_url + '/executor/case/document/type/cause' , postDocCase , options).subscribe(function (res) {
      thisref.cases = res.json()['radio'];
      thisref.test1 = res['test_st'];
    })

  }

  public selectedPersonType;

  onSelectionPaymentChange(type:string){
    this.selectedPersonType = type;

    // this.selectedPaymentEntry = type;
    // this.selectedPayModeID = type.type;
    // this.idselected = type.selected;
  }



   loadPdf(): Observable<any> {
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'text/plain');
    headers.append('Content-Type', 'application/json');

    let DocParams = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_doc_type : thisref.id_doc_type,
      id_case : toInt(thisref.generateDosarID)
    }
    return this.http.post(this.main_url + '/executor/case/document/template/load', DocParams, {headers: headers})
      .map(result => {
        return result;
      })
  }


   loadTemplate() {

    var thisref = this;

    this.loadPdf().subscribe((res)=>{



      thisref.ckeditorContent = res['_body'];
      thisref.ckeditorContent = thisref.ckeditorContent.replace('$(cause)',thisref.document_cause+'');
      if(thisref.selected['name'] == 'Preluare'){
        thisref.ckeditorContent = thisref.ckeditorContent.replace('$(date_come)',thisref.coming_date.toLocaleDateString('ru-RU')+'');
        thisref.ckeditorContent = thisref.ckeditorContent.replace('$(time)',thisref.coming_time+'');

      }
    })
     thisref.show_topBar = false;
  }


  public created_date_case : any;
  ngOnInit() {
    jQuery('.ng-tns-c3-0 .ui-panel .ui-widget .ui-widget-content .ui-corner-all').css('padding','0');
    jQuery('#cred').css('padding-top','3%')
    var thisref = this;


    this.route.params.subscribe(function(params){
      thisref.idurl = params['name'];

    });

    let test = JSON.parse(localStorage.getItem('first'));
    this.route.params.subscribe(function(params){
      thisref.generateDosarID = params['name'];

    });
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : toInt(GlobalVariable.login_id),
      id_case : toInt(thisref.generateDosarID)
    };
    this.http.post(thisref.main_url + '/executor/case/details' , postParams , options).subscribe(function (res) {
      thisref.case_number = res.json()['case_number'];
      thisref.created_date_case = res.json()['created_date']
      thisref.case_array.push(res.json()['case_number']);


    })

    }
  ngAfterViewInit(): void {
    jQuery('.ng-tns-c3-0 .ui-panel .ui-widget .ui-widget-content .ui-corner-all').css('padding','0');
    jQuery('#cred').css('padding-top','3%')
  }
}
