import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpezeComponent } from './speze.component';

describe('SpezeComponent', () => {
  let component: SpezeComponent;
  let fixture: ComponentFixture<SpezeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpezeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpezeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
