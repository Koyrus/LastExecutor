import {AfterViewInit, Component, ElementRef, Input, OnInit, Renderer2, VERSION, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ConfirmationService, DataTable, FileUpload, Message} from "primeng/primeng";
import {Http, RequestOptions, Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {Observable} from "rxjs/Observable";
import {IMyDpOptions} from "mydatepicker";
import {MatDatepicker} from "@angular/material/datepicker";
import {LocaleService} from "mydatepicker/dist";
import {LocaleServiceDate} from "../LocaleService";
import {DashboardComponent} from "../dashboard/dashboard.component";
import {RoleService} from "../role-service";
import {MessageService} from "primeng/components/common/messageservice";
import {DatePipe} from "@angular/common";


@Component({
  selector: 'app-edit-dosare-pensii-alimentare',
  templateUrl: './edit-dosare-pensii-alimentare.component.html',
  styleUrls: ['./edit-dosare-pensii-alimentare.component.css'],


})


export class EditDosarePensiiAlimentareComponent implements OnInit, AfterViewInit {
  @ViewChild(MatDatepicker) datepicker: MatDatepicker<Date>;
  @ViewChild('fileInput') el: ElementRef;
  @ViewChild("cerere_file") cerere_file: FileUpload;
  @Input() case_id_edit: number = 0;
  @Input() doneBtnText = '123';
  @Input() nextBtnText = '345';
  @Input() previousBtnText = '567';

  formatsDateTest: string[] = ['dd/MM/yyyy'];

  public dateNow: any;
  public usdVal: number;
  public eurVal: number;
  date6: Date;
  public testUsers : any[] = [
    {name:'test',surname : 'test22' , age : '6'},
    {name:'test2',surname : 'test23' , age : '7'},
    {name:'test3',surname : 'test24' , age : '5'},
    {name:'test4',surname : 'test25' , age : '1'},
    {name:'test5',surname : 'test26' , age : '-1'}

  ]


  /* dateNowISO = this.dateNow.toISOString();
   dateNowMilliseconds = this.dateNow.getTime();*/


  public childId : number;

//////////////////////////Добавление/Изменение детей////////////////////////
public childSelected:any;


  public showinputfields : boolean;
  public isAddChild:boolean;

  onRowSelect(event) {
    this.isAddChild = false;
    this.newChildObject ={
      'first_name': event.data.first_name,
      'last_name':  event.data.last_name,
      'idnp': event.data.idnp,
      'birth_date': new Date(event.data.birth_date),
      'majority_date': '',
      'mdl': 0,
      'usd': 0,
      'eur': 0,
      'total' : 0,
      'age':0,
      'is_delete':false
    }
    this.showinputfields = true;
  }




  // cloneCar(c: Car): Car {
  //   let car = new PrimeCar();
  //   for(let prop in c) {
  //     car[prop] = c[prop];
  //   }
  //   return car;
  // }
  //
  public childDeleteArray : any[]=[];
  confirmDelete(con : any){
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți?',
      header: 'Confirmare',
      icon: 'fa fa-question-circle',
      accept: () => {
        let index = this.childrens.indexOf(con);

        this.msgsChild = [{severity:'info', summary:'Confirmed', detail:'You have accepted'}];
        con.is_delete = true;
        if(con.case_part_id != '' || con.case_part_id != null){
          this.childDeleteArray.push(con);
        }
        this.childrens = this.childrens.filter((val, i) => i != index);
        this.showinputfields = false;

      },
      reject: () => {
        this.msgsChild = [{severity:'info', summary:'Rejected', detail:'You have rejected'}];

      }
    });
  }

  public disableButtonSave : boolean = true;

  checkIfEmptyField(){
    var thisref = this;
    if(this.newChildObject['last_name'].length >= 1  && this.newChildObject['first_name'].length >= 1) {
      this.disableButtonSave = false;
    }else {
      thisref.disableButtonSave = true;

    }
  }

  adaugaCopil() {

    let childrenstemp = [...this.childrens];

    let dataNasterii = this.newChildObject.birth_date;
    this.newChildObject.age = this.dateNow.getFullYear() - dataNasterii.getFullYear();
    this.addMajoriytDate();
    if (this.isAddChild) {
      childrenstemp.push(this.newChildObject);
    } else {
      childrenstemp[this.findSelectedChildIndex()] = this.newChildObject;
    }
    this.childrens = childrenstemp;
    this.newChildObject = null;
    this.showinputfields = false;
  }

  findSelectedChildIndex(): number {
    return this.childrens.indexOf(this.childSelected);
  }


  showInputsChild(){
    this.isAddChild=true;

    this.newChildObject  = {
      'first_name': '',
      'last_name': '',
      'idnp': '',
      'birth_date': '',
      'majority_date': '',
      'mdl': 0,
      'usd': 0,
      'eur': 0,
      'total' : 0,
      'age':0,
      'case_part_id':null
    };

    this.showinputfields = true;
  }


//////////////////////////////////////




  ngAfterViewInit(): void {
    jQuery('.card-footer').css('background-color', '#4a89dc');
    jQuery('.card-footer').css('text-align', 'right');
    jQuery('.card-header').css({'font-size': 'medium'});
    // jQuery('.ui-panel-content-wrapper .ng-trigger .ng-trigger-panelContent').css({'background-color':'red'})
    jQuery('.ui-button-icon-only .fa').css('left','48%')
    var buttonPrev = jQuery('.card-footer button:first').text('Precedent');
    var buttonNext = jQuery('.card-footer button:first').next().text('Următorul');
    var buttonPrev2 = jQuery('.card-footer button:last').text('Salvează');


    jQuery('.ui-inputtext').css({
      'border': 'solid 1px',
      'border-color': '#999999',
      'padding-left': '5px',
      'height': '2rem',
      'width': '100%'
    });
    jQuery('.mydp').css({'width': '60%'})

  }

  public case_team: any[] = [];
  public current_date: any;
  public amount_eur: number ;
  public amount_mdl: number ;
  public amount_usd: number ;
  public amount: number ;

  public myDatePickerOptions: IMyDpOptions;
  // = this.dateService.getLocaleOptions();
  // {
  // dayLabels: {su: "du", mo: "lu", tu: "ma", we: "mi", th: "jo", fr: "vi", sa: "sa"},
  // monthLabels: { 1: "ian", 2: "feb", 3: "mart", 4: "apr", 5: "mai", 6: "iun", 7: "iul", 8: "aug", 9: "sept", 10: "oct", 11: "nov", 12: "dec" },
  // dateFormat: "dd.mm.yyyy",
  // todayBtnTxt: "Astăzi",
  // firstDayOfWeek: "mo",
  // sunHighlight: true,

  // };

  msgsChild: Message[] = [];

  public created_date;
  public emitDate: Date;
  public init_date: any;
  public testDate: Date;
  public phoneCreditor : string;
  public phoneDebitor : string;
  // public model: any = current.ge;
  public emit_date: any;

  constructor(private http: Http, private router: Router, private route: ActivatedRoute, private dateService: LocaleServiceDate, private dash: DashboardComponent, private roleService: RoleService  ,private messageService: MessageService , private datePipe: DatePipe , private confirmationService: ConfirmationService) {
    // this.datepicker
    this.myDatePickerOptions = dateService.getLocaleOptions()


    var thisref = this;
    this.route.params.subscribe(function (params) {
      thisref.editDosarID = params['name'];
    });


    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case_pa: null
    };

    if (thisref.editDosarID > 0) {
      postParams.id_case_pa = toInt(thisref.editDosarID)
    } else {
      postParams.id_case_pa = null;
    }


    this.http.post(this.main_url + "/executor/pa/case/create/edit", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.case_numberPA = res.json()['case_number'];
      thisref.case_team = res.json()['case_team'];

      thisref.fillData(res.json(), thisref);

    });
  }



  selectedEntry1;
  public case_numberPA: string;
  // public baseApiUrl = GlobalVariable.globalApiUrl;
  public state_case: string;
  //ID кейса///
  public editDosarID: number = null;
  public state_dosar: any[] = [];
  public id: number;
  public myText: string = '';
  public myTextCreditor: string = '';
  public myTextChilds: string = '';
  msgs: Message[];
  msgsInst : Message[];
  public disable_file: boolean = true;
  public childs: any[] = [];
  public dosarName: string = null;
  public childrens: any[] = [];

  public display: boolean = false;
  public pustoi_array: string;
  public index: number = 1;
  public file_name: any;
  public payment_mode: any[] = [];
  public priority_mode: any[] = [];
  public arrayForAutocompleteDebitor: any[] = [];
  public responseChildIDNP: any[] = [];
  public id_file_type: number = 1;
  public url_for_upload = 'http://192.168.1.69:8080/user/case/fileupload/' + this.id_file_type;
  public main_url = GlobalVariable.globalApiUrl;

  //FileUpload!!!!
  public choose_file_type: any[] = [];
  public url: string = 'http://192.168.1.69:8080/user/case/fileupload';
  @Input() multiple: boolean = false;
  @ViewChild('fileInput') inputEl: ElementRef;
  public uploadCaseFiles: File[] = [];
  public uploadedContent: any[] = [];
  /////////////////////////////////////////
  doc_executor: any;
  inst_emit_id: any;
  /////////////////Creditor///////////////////
  public id_creditor: number;
  public idnp_creditor: any;
  public id_city_creditor: number = 1;
  public first_name_creditor: string;
  public last_name_creditor: string;
  public patronimic_creditor: string;
  public birth_date_creditor: Date;
  public post_code_creditor: string;
  public city_creditor: string;
  public street_creditor: string;
  public adresa_domiciliuCreditor : string;
  public adresa_domiciliuDebitor : string;
  public block_creditor: string;
  /////////////////Debitor///////////////////
  public id_debitor: number;
  public idnp_debitor: any;
  public id_city_debitor: number = 1;
  public first_name_debitor: string;
  public last_name_debitor: string;
  public patronimic_debitor: string;
  public birth_date_debitor: Date;
  public post_code_debitor: string;
  public city_debitor: string;
  public street_debitor: string;
  public block_debitor: string;

  public validIDNp: boolean = false;
  public stateType: boolean;
  public revenue: string[] = ['1', '1/4', '1/3', '1/2'];
  date_from : Date;
  date_to;
  end_date_case : Date;
  date;
  public emitent_instance: any[] = [];
  public payment_instance: any[] = [];
  public priority_instance: any[] = [];
  document_type: any;
  sum: any;
  text_procedura: any;
  stateCase: string = 'Initiere';
  public emitent_resultAutoComplete: string[] = [];
  text: string;
  filteredBrands: any[];
  public selectedPriorityEntry;
  public selectedPaymentEntry;

  public loader = false; // show loading


  onSelectionChangePriority(priority) {
    this.selectedPriorityEntry = priority;
    this.selectedPriorityID = priority.id;

  }

  public allPaymentTypes: boolean = true;

  onSelectionPaymentChange(item) {
    var thisref = this;

    this.selectedPaymentEntry = item;
    this.selectedPayModeID = item.id;
    this.allPaymentTypes = true;

    if (this.selectedPayModeID == 32) {
      this.allPaymentTypes = false;
    }
  }
  public showDialogIfInstNotFound : boolean;
  filterBrands(event) {
    this.filteredBrands = null;
    var thisref = this;
    if (this.emitent_instance.length > 1) {
      this.http.get(this.main_url + '/executor/case/instance/search/' + this.emitent_instance).subscribe(function (res) {
        thisref.emitent_resultAutoComplete = res.json();
        if(res.json() == [] || res.json() == ''){
          thisref.emitent_instance = [];
          thisref.showDialogIfInstNotFound = true;
          thisref.msgsInst = [];
          thisref.msgsInst.push({severity:'error', detail:'Instituția Emitentă nu a fost găsită'});
        }
      });
    }
  }




  onStep1Next() {

  }

  myFuncDebitorIDNP() {
    this.myText = this.idnp_debitor.length;


    if (this.idnp_debitor.length == 13) {


      let validNumber = toInt(this.idnp_debitor);

      this.validIDNp = true;

      var thisref = this;
      // setTimeout(() => {


        let postData = {
          idnp: toInt(this.idnp_debitor),
          id_login_user: toInt(GlobalVariable.login_id),
        };



         this.http.post(this.main_url + '/executor/case/idnp/search', postData, this.roleService.getHeadersOption()).subscribe(function (res) {
           thisref.arrayForAutocompleteDebitor = res.json();
           thisref.loader = true;

           if (thisref.arrayForAutocompleteDebitor['id'] != null) {

             thisref.arrayForAutocompleteDebitor = res.json();
            thisref.id_debitor = thisref.arrayForAutocompleteDebitor['id'];
            thisref.idnp_debitor = thisref.arrayForAutocompleteDebitor['idnp'];
            thisref.adresa_domiciliuDebitor = thisref.arrayForAutocompleteDebitor['home_address']
            //  thisref.id_city_debitor = thisref.arrayForAutocompleteDebitor['id_city'];
            thisref.id_city_debitor = 1;
            thisref.phoneDebitor = thisref.arrayForAutocompleteDebitor['phone'];
            thisref.first_name_debitor = thisref.arrayForAutocompleteDebitor['first_name'];
            thisref.last_name_debitor = thisref.arrayForAutocompleteDebitor['last_name'];
            thisref.patronimic_debitor = thisref.arrayForAutocompleteDebitor['patronimic'];
            thisref.birth_date_debitor = new Date(thisref.arrayForAutocompleteDebitor['birth_date']);
            thisref.post_code_debitor = thisref.arrayForAutocompleteDebitor['post_code'];
            thisref.city_debitor = thisref.arrayForAutocompleteDebitor['city'];
            thisref.street_debitor = thisref.arrayForAutocompleteDebitor['street'];
            thisref.block_debitor = thisref.arrayForAutocompleteDebitor['block'];
             thisref.loader = false;

          } else {
            thisref.id_debitor = null;
            thisref.stateType = false;
             thisref.loader = false;
             thisref.adresa_domiciliuDebitor = null;
             //  thisref.id_city_debitor = thisref.arrayForAutocompleteDebitor['id_city'];
             thisref.id_city_debitor = 1;
             thisref.phoneDebitor = null;
             thisref.patronimic_debitor= null;
             thisref.post_code_debitor = null;
             thisref.city_debitor = null;
             thisref.street_debitor = null;
             thisref.block_debitor = null;

          }


         })

      // }, 0);

    } else {

      if (this.validIDNp) {
        this.clearAllfieldsDebitor(this.index);
        this.validIDNp = false
      }
    }

  }

  public dateB: any;
  public monthB: any;
  public dayB: any;
  public dividedRes: number;
  public dividedResToFix: any;

  nextAchitari() {
    // var thisref = this;
    // this.disablefirstInputPayment = true;
    //
    //
    // if (thisref.childrens.filter(child => child.first_name != '').length == 1) {
    //   this.dividedRes = this.resultAmmountInit / 4;
    //   this.dividedResToFix = this.dividedRes.toFixed(2)
    // }
    // if (thisref.childrens.filter(child => child.first_name != '').length == 2) {
    //   this.dividedRes = this.resultAmmountInit / 3;
    //   this.dividedResToFix = this.dividedRes.toFixed(2)
    //
    //
    // }
    // if (thisref.childrens.filter(child => child.first_name != '').length >= 3) {
    //   this.dividedRes = this.resultAmmountInit / 2;
    //   this.dividedResToFix = this.dividedRes.toFixed(2)
    //
    //
    // }
    //
    // // for (let i = 0; i < this.childrens.length; i++) {
    // //   let birth_date_temp = this.childrens[i]['birth_date'];
    // //   if (birth_date_temp != '') {
    // //     var date = new Date(birth_date_temp);
    // //     var date1 = date.getFullYear() + 18;
    // //     var dateDay = date.getDay();
    // //     var dateMonth = date.getMonth();
    // //     this.monthB = dateMonth;
    // //     this.dateB = date1;
    // //     this.dayB = dateDay;
    // //
    // //     date.setFullYear(date1);
    // //     // this.childrens[i]['majority_date'] = date.toLocaleDateString('en-GB');
    // //   }
    // //
    // // }
  }

  addMajoriytDate() {

    var date = this.newChildObject.birth_date;
    // date.toLocaleDateString('ru-RU');
    // date.setFullYear(date.getFullYear() + 18);

    var year = date.getFullYear();

    var dateDay = date.getDate();
    var dateMonth = date.getMonth();
    var dateTemp = new Date();

    dateTemp.setFullYear(date.getFullYear()+18);
    dateTemp.setDate(dateDay);
    dateTemp.setMonth(dateMonth);

    //

    this.newChildObject.majority_date = dateTemp.toLocaleDateString('en-GB')
  }

  myFuncChildrensIDNP() {
    var thisref = this;

    // let childData = thisref.childrens[this.index - 1];

    let idnp_dat = this.newChildObject.idnp;
    thisref.myTextChilds = idnp_dat.length;

    if (idnp_dat.length == 13) {
      setTimeout(() => {


        let postData = {
          idnp: toInt(idnp_dat),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.main_url + '/executor/case/idnp/search', postData, this.roleService.getHeadersOption()).subscribe(function (res) {
          thisref.responseChildIDNP = res.json();
          if (thisref.responseChildIDNP['id'] != null && thisref.responseChildIDNP['id'] != '') {
            thisref.newChildObject['last_name'] = thisref.responseChildIDNP['last_name'];
            thisref.newChildObject['idnp'] = thisref.responseChildIDNP['idnp'];
            thisref.newChildObject['first_name'] = thisref.responseChildIDNP['first_name'];
            thisref.newChildObject['birth_date'] = new Date(thisref.responseChildIDNP['birth_date']);
            thisref.newChildObject['id'] = thisref.responseChildIDNP['id'];
            thisref.newChildObject['mdl'] = 0;
            thisref.newChildObject['usd'] = 0;
            thisref.newChildObject['eur'] = 0;
            thisref.disableButtonSave = false;
          } else {
            thisref.newChildObject['id'] = null;
            thisref.newChildObject['case_part_id'] = null;

          }
        })
      }, 1500);
    }
  }

  myFuncCreditorIDNP() {
    this.myTextCreditor = this.idnp_creditor.length;


    if (this.idnp_creditor.length == 13) {
      this.validIDNp = true;
      var thisref = this;
      setTimeout(() => {

        let postData = {
          idnp: toInt(this.idnp_creditor),
          id_login_user: toInt(GlobalVariable.login_id),
        };


        this.http.post(this.main_url + '/executor/case/idnp/search', postData, this.roleService.getHeadersOption()).subscribe(function (res) {
          thisref.arrayForAutocompleteDebitor = res.json();
          if (thisref.arrayForAutocompleteDebitor['id'] != null && thisref.arrayForAutocompleteDebitor['id'] != '') {
            thisref.id_creditor = thisref.arrayForAutocompleteDebitor['id'];
            thisref.idnp_creditor = thisref.arrayForAutocompleteDebitor['idnp'];
            // thisref.id_city_creditor = thisref.arrayForAutocompleteDebitor['id_city'];
            thisref.id_city_creditor = 1;
            thisref.phoneCreditor = thisref.arrayForAutocompleteDebitor['phone'];

            thisref.adresa_domiciliuCreditor = thisref.arrayForAutocompleteDebitor['home_address']
            thisref.first_name_creditor = thisref.arrayForAutocompleteDebitor['first_name'];
            thisref.last_name_creditor = thisref.arrayForAutocompleteDebitor['last_name'];
            thisref.patronimic_creditor = thisref.arrayForAutocompleteDebitor['patronimic'];
            thisref.birth_date_creditor = new Date(thisref.arrayForAutocompleteDebitor['birth_date']);
            thisref.post_code_creditor = thisref.arrayForAutocompleteDebitor['post_code'];
            thisref.city_creditor = thisref.arrayForAutocompleteDebitor['city'];
            thisref.street_creditor = thisref.arrayForAutocompleteDebitor['street'];
            thisref.block_creditor = thisref.arrayForAutocompleteDebitor['block'];
            // thisref.stateType = true;
          } else {
            thisref.id_creditor = null;
            thisref.stateType = false;
            thisref.stateType = false;
            thisref.loader = false;

            thisref.id_city_creditor = 1;
            thisref.phoneCreditor = null;

            thisref.adresa_domiciliuCreditor = null;
            thisref.patronimic_creditor = null;
            thisref.post_code_creditor = thisref.arrayForAutocompleteDebitor['post_code'];
            thisref.city_creditor = null;
            thisref.street_creditor = null;
            thisref.block_creditor = null;
          }
        })
      }, 1500);
    } else {

      if (this.validIDNp) {
        this.clearAllfields(this.index);
        this.validIDNp = false;

      }
    }

  }

  clearAllfields(index: number) {
    var thisref = this;
    thisref.id_creditor = null;
    thisref.id_city_creditor = null;
    thisref.first_name_creditor = null;
    thisref.last_name_creditor = null;
    thisref.patronimic_creditor = null;
    thisref.birth_date_creditor = null;
    thisref.post_code_creditor = null;
    thisref.city_creditor = null;
    thisref.street_creditor = null;
    thisref.block_creditor = null;

    ////////////////////////////


  }

  clearAllfieldsDebitor(index: number) {
    var thisref = this;

    thisref.id_debitor = null;
    thisref.id_city_debitor = null;
    thisref.first_name_debitor = null;
    thisref.last_name_debitor = null;
    thisref.patronimic_debitor = null;
    thisref.birth_date_debitor = null;
    thisref.post_code_debitor = null;
    thisref.city_debitor = null;
    thisref.street_debitor = null;
    thisref.block_debitor = null;
  }

  achitari() {
  }

  onSelectFile() {

    let tempPostTypeDocs = {
      'id_document': null,
      'file_name': this.cerere_file.files[0].name,
      'doc_type': this.choose_file_type['name'],
      'id_doc_type': this.choose_file_type['id'],
      'is_delete': false
    };
    this.uploadedContent.push(tempPostTypeDocs);
    this.uploadCaseFiles.push(this.cerere_file.files[0]);
    this.cerere_file.clear();
  }

  deleteCaseFileFromArray(file_select: any) {
    if (file_select.id_document != null && file_select.id_document != '') {
      for (var y = 0; y < this.uploadedContent.length; y++) {
        let file_elem = this.uploadedContent[y];
        if (file_elem.id_document === file_select.id_document) {
          file_elem.is_delete = true;
        }
      }
    } else {
      this.uploadedContent = this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
    }
  }

  selectedTeamEntry;
  selectedTeamID: number = null;
  public array_team_members: any[] = [];
  public unique: any[] = [];

  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  changeTeamMembers(team) {
    this.selectedTeamEntry = team;
    this.selectedTeamID = team.id;
    this.selectedTeamEntry['is_selected'] = true;
    this.array_team_members.push(this.selectedTeamEntry);
    for (let i = 0; i < this.array_team_members.length; i++) {
      var a = this.array_team_members;
      this.unique = a.filter(EditDosarePensiiAlimentareComponent.onlyUnique);
    }
  }


  onChangeFunction() {
    if (this.choose_file_type != null) {
      this.disable_file = false;
    }
  }



  public ro : any;
  public maxDateDosar : Date;
  ngOnInit() {
    this.maxDateDosar = new Date();

    jQuery('.ui-datatable-even .ui-widget-content').css({'background-color':'red'})
    this.newChildObject  = {
      'first_name': '',
      'last_name': '',
      'idnp': '',
      'birth_date': new Date(),
      'majority_date': '',
      'mdl': 0,
      'usd': 0,
      'eur': 0,
      'total' : 0
    };
    this.dateNow = new Date();
    this.ro = {
      firstDayOfWeek: 1,
      dayNames: [ "duminica","luni","marți","miercuri","joi","vineri","sîmbăta" ],
      dayNamesShort: [ "du","lu","ma","mi","jo","vi","sa" ],
      dayNamesMin: [ "D","L","M","M","J","V","S" ],
      monthNames: [ "ianuarie","februarie","martie","aprilie","mai","iunie","iulie","august","septembrie","octombrie","noiembrie","decembrie" ],
      monthNamesShort: [ "ian","feb","mart","apr","mai","iun","iul","aug","sept","oct","nov","dec" ],
      today: 'Astăzi',
      clear: 'Borrar'
    }






    this.date = new Date();
    var thisref = this;
    this.http.get(this.main_url + '/executor/case/nomenclator/paymode', {}).subscribe(function (response) {
      thisref.payment_mode = response.json();

    });

    this.http.get(this.main_url + '/executor/case/nomenclator/priority').subscribe(function (res) {
      thisref.priority_mode = res.json()
    });

    this.http.get(this.main_url + '/executor/case/nomenclator/doctype').subscribe(function (res) {
      thisref.document_type = res.json();
      thisref.choose_file_type = thisref.document_type[0];

    });

    this.route.params.subscribe(function (params) {
      thisref.dosarName = params['dosarName'];
    });
    // for (let i = 1; i < 5; i++) {
    //   var text = {
    //     'order_id': i,
    //     'first_name': '',
    //     'last_name': '',
    //     'idnp': '',
    //     'birth_date': new Date(),
    //     'majority_date': '',
    //     'mdl': 0,
    //     'usd': 0,
    //     'eur': 0,
    //     'total' : 0
    //   };
    //   this.childrens.push(text);
    // }



    // this.pustoi_array = this.childrens[0].name;
  }
  public personalPayment : any[]=[];

  public totalPersonalPayments : number = 0;

  calcPersonal(total : any){
    var thisref = this;
    var data = [];
    var sumMdl = 0;
    var sumUsd = 0;
    var sumEur = 0;
    var sumTotalPersonal = 0;
    for(let i = 0;i < thisref.childrens.filter(child => child.first_name != '').length;i++){
      data.push({id : i , mdlValue : thisref.childrens[i].mdl , usdValue : thisref.childrens[i].usd , eurValue : thisref.childrens[i].eur});
      sumMdl += thisref.childrens[i].mdl;
      sumUsd += thisref.childrens[i].usd * 16;
      sumEur += thisref.childrens[i].eur * 20;
      sumTotalPersonal = sumMdl + sumUsd + sumEur;
      this.totalPersonalPayments = toInt(sumTotalPersonal)

    }
    thisref.personalPayment = data;
  }


  public resultAmmount: number;
  public resultAmmountInit: number = 0;
  public mdlVal: number;
  public dividedResInit: number;
  public disablefirstInputPayment: boolean;

  calculate() {
    var thisref = this;
    this.disablefirstInputPayment = false;
    this.usdVal = this.amount_usd * 16;
    this.eurVal = this.amount_eur * 20;
    this.mdlVal = this.amount_mdl * 1;

    this.resultAmmount = this.usdVal + this.eurVal + this.mdlVal;
    if (thisref.childrens.filter(child => child.first_name != '').length === 1) {
      this.dividedResInit = this.resultAmmount / 4;
    }
    else if (thisref.childrens.filter(child => child.first_name != '').length === 2) {
      this.dividedResInit = this.resultAmmount / 3;
    }
    else if (thisref.childrens.filter(child => child.first_name != '').length >= 3) {
      this.dividedResInit = this.resultAmmount / 2;
    }

  }

  onBasicUpload() {
  }
  displayDialog: boolean;
  newCar: boolean;

  showDialogToAdd() {
    this.newCar = true;
    this.displayDialog = true;
  }

  onStepDebitor() {

  }

  onStepCreditor() {
    this.index = 1;
  }

  onComplete() {
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});

    for (let child of this.childrens) {
      if (child.first_name != "") {

        this.childs.push(child);

      }
    }


    let postParams = {
      id: this.id,
      id_login_user: toInt(GlobalVariable.login_id),
      case_number: this.case_numberPA,
      ////Step1///
      state_case: this.state_case,
      nom_emitent_instance: this.emitent_instance,
      nom_pay_mode: this.selectedPaymentEntry,
      nom_priority: this.selectedPriorityEntry,
      doc_executor: this.doc_executor,
      case_date: this.created_date,
      doc_executor_date: this.emitDate,
      ////Step2///
      debitor: {
        id: this.id_debitor,
        idnp: this.idnp_debitor,
        // id_city : this.id_city_debitor,
        id_city: 1,
        first_name: this.first_name_debitor ,
        last_name: this.last_name_debitor,
        patronimic: this.patronimic_debitor != null ? this.patronimic_debitor : null,
        birth_date: this.birth_date_debitor,
        city: this.city_debitor != null ? this.city_debitor : null,
        street: this.street_debitor != null ? this.street_debitor : null,
        block: this.block_debitor != null ? this.block_debitor : null,
        post_code: this.post_code_debitor != null ? this.post_code_debitor : null,
        phone : this.phoneDebitor != null ? this.phoneDebitor : null,
        home_address : this.adresa_domiciliuDebitor != null ? this.adresa_domiciliuDebitor : null
      },
      ////Step3///
      creditor: {
        id: this.id_creditor,
        idnp: this.idnp_creditor,
        // id_city : this.id_city_creditor,
        id_city: 1,
        first_name: this.first_name_creditor,
        last_name: this.last_name_creditor,
        patronimic: this.patronimic_creditor != null ? this.patronimic_creditor : null ,
        birth_date: this.birth_date_creditor,
        city: this.city_creditor,
        street: this.street_creditor != null ? this.street_creditor : null ,
        block: this.block_creditor != null ? this.block_creditor : null ,
        post_code: this.post_code_creditor != null ? this.post_code_creditor : null ,
        phone : this.phoneCreditor != null ? this.phoneCreditor : null ,
        home_address : this.adresa_domiciliuCreditor != null ? this.adresa_domiciliuCreditor : null
      },
      ////Step4///
      children: this.childrens.concat(this.childDeleteArray),
      ////Step5///
      date_from: this.date_from != null ? this.date_from : null,
      // amount : parseFloat(this.sum),
      amount_eur: this.amount_eur != null ? this.amount_eur : null,
      amount_mdl: this.amount_mdl != null ? this.amount_mdl : null,
      amount_usd: this.amount_usd != null ? this.amount_usd : null,

      ////Step6///
      expired_date_case: this.end_date_case != null ? this.end_date_case : null,
      doc_type: this.choose_file_type,
      text_procedure: this.text_procedura != null ? this.text_procedura : null,
      case_team: this.unique,
      files_content: this.uploadedContent
    };


    this.sendDatePAcase(postParams).subscribe(() => {
      this.loader = false;
      this.router.navigateByUrl("dash/dosarePensiiAlimentare");
    });


  }

  sendDatePAcase(stepsData: any): Observable<any> {
    var headers = new Headers();
    var form = new FormData();
    for (let i = 0; i < this.uploadCaseFiles.length; i++) {
      form.append("file", this.uploadCaseFiles[i]);
    }
    form.append('case_pa', new Blob([JSON.stringify(stepsData)], {
      type: "application/json"
    }));
    this.loader = true;
    return this.http.post(this.main_url + '/executor/pa/case/save', form, {headers: headers})
      .map(result => {

        return result;
      })
  }

  /// with //AAA333///
  /*sendDatePAcase(stepsData : any): void {
    var headers = new Headers();
    let options = new RequestOptions({ headers: headers });
    var form = new FormData();
    for(let i = 0;i < this.uploadCaseFiles.length ; i++){
      form.append("file",  this.uploadCaseFiles[i]);
    }
    form.append('case_pa', new Blob([JSON.stringify(stepsData)], {
      type: "application/json"
    }));
    this.http.post(this.main_url+'/executor/pa/case/save' , form, {
      headers: headers
    }).subscribe(function (res) {

    });
    setTimeout(()=>{  this.router.navigateByUrl("dash/dosarePensiiAlimentare");    },3000);


  }*/


  public setInst: any[] = [];
  public selectedPayModeID: number;
  public selectedPriorityID: number;
  public created_PA_date: string;
  public child_array: any[] = [];
  public newChildObject : any;


  // adaugaCopil(dt : DataTable){
    // let casePartID = this.newChildObject['case_part_id'];
    // if(casePartID == null || casePartID == ''){
    //
    //   let  dataNasterii = new Date( this.newChildObject['birth_date']);
    //   let age = this.dateNow.getFullYear() - dataNasterii.getFullYear();
    //   /// thisref.childrens[m]['order_id'] = m + 1;
    //
    //   this.newChildObject.age=age;
    //   this.newChildObject.name=this.newChildObject['first_name']+' '+this.newChildObject['last_name'];
    //
    //   this.childrens.push(this.newChildObject);
    //   // dt.reset();
    // } else {
    //   // for(let i = 0;i < this.childrens.length;i++){
    //   //   if(this.childrens[i]['case_part_id'] == casePartID){
    //   //
    //   //
    //   //
    //   //     this.childrens[i]=this.newChildObject;
    //   //      break;
    //   //   }
    //   // }
    //
    // }
  //   this.showinputfields = false;
  //
  //
  //
  //
  // }
  cancel(){
    this.showinputfields = false;
  }
  fillData(casePAEdit: any[], thisref: any) {
    // let currentTime = Date.now();


    thisref.state_case = casePAEdit['state_case'];

    thisref.created_date = new Date(casePAEdit['case_date']);


    if (casePAEdit['id'] > 0) {
      thisref.id = casePAEdit['id'];
      ////First page////
      thisref.uploadedContent = casePAEdit['files_content'];
      thisref.emitent_instance = casePAEdit['nom_emitent_instance'];
      thisref.doc_executor = casePAEdit['doc_executor'];
      thisref.date_from = new Date(casePAEdit['date_from'])
      ///Debitor Page///
      thisref.id_debitor = casePAEdit['debitor']['id'];
      thisref.idnp_debitor = casePAEdit['debitor']['idnp'];
      thisref.first_name_debitor = casePAEdit['debitor']['first_name'];
      thisref.last_name_debitor = casePAEdit['debitor']['last_name'];
      thisref.patronimic_debitor = casePAEdit['debitor']['patronimic'];
      thisref.city_debitor = casePAEdit['debitor']['city'];
      thisref.street_debitor = casePAEdit['debitor']['street'];
      thisref.block_debitor = casePAEdit['debitor']['block'];
      thisref.birth_date_debitor = new Date(casePAEdit['debitor']['birth_date']);
      thisref.post_code_debitor = casePAEdit['debitor']['post_code'];
      thisref.phoneDebitor = casePAEdit['debitor']['phone'];
      thisref.adresa_domiciliuDebitor = casePAEdit['debitor']['home_address'];
      ///Creditor/////
      thisref.id_creditor = casePAEdit['creditor']['id'];
      thisref.idnp_creditor = casePAEdit['creditor']['idnp'];
      thisref.first_name_creditor = casePAEdit['creditor']['first_name'];
      thisref.last_name_creditor = casePAEdit['creditor']['last_name'];
      thisref.patronimic_creditor = casePAEdit['creditor']['patronimic'];
      thisref.city_creditor = casePAEdit['creditor']['city'];
      thisref.street_creditor = casePAEdit['creditor']['street'];
      thisref.block_creditor = casePAEdit['creditor']['block'];
      thisref.birth_date_creditor = new Date(casePAEdit['creditor']['birth_date']);
      thisref.post_code_creditor = casePAEdit['creditor']['post_code'];
      thisref.phoneCreditor = casePAEdit['creditor']['phone'];
      thisref.adresa_domiciliuCreditor = casePAEdit['creditor']['home_address'];
      thisref.childrens = casePAEdit['children'];
      thisref.amount_usd = casePAEdit['amount_usd'];
      thisref.amount_mdl = casePAEdit['amount_mdl'];
      thisref.amount_eur = casePAEdit['amount_eur'];
      thisref.usdVal = thisref.amount_usd * 16;
      thisref.eurVal = thisref.amount_eur * 20;
      thisref.resultAmmountInit = thisref.usdVal + thisref.eurVal + thisref.amount_mdl;

      // thisref.child_array.push(thisref.childrens);
      thisref.testDate = new Date(casePAEdit['case_date']);
      thisref.emitDate = new Date(casePAEdit['doc_executor_date']);



      thisref.validIDNp = true;
      let totalChildren = thisref.childrens.length;

      for (var m = 0; m < totalChildren; m++) {
        let  dataNasterii = new Date( thisref.childrens[m]['birth_date']);
        let age = this.dateNow.getFullYear() - dataNasterii.getFullYear();
       /// thisref.childrens[m]['order_id'] = m + 1;

        thisref.childrens[m].index=m+1;

        thisref.childrens[m].age=age;


        thisref.childrens[m]['birth_date'] = new Date( thisref.childrens[m]['birth_date'])
      }

      /////////Achitari////
      //    thisref.setInst = casePAEdit['nom_pay_mode']['name'];
      thisref.selectedPayModeID = casePAEdit['nom_pay_mode']['id'];
      thisref.selectedPaymentEntry = casePAEdit['nom_pay_mode'];
      thisref.date_from = new Date(casePAEdit['date_from']);
      thisref.date_to = casePAEdit['date_to'];
      thisref.sum = casePAEdit['amount'];
      ////////////Prioritate/////////////
      thisref.selectedPriorityEntry = casePAEdit['nom_priority'];
      thisref.selectedPriorityID = casePAEdit['nom_priority']['id'];
      thisref.end_date_case = new Date(casePAEdit['expired_date_case']);
      thisref.text_procedura = casePAEdit['text_procedure'];
      // public selectedPriorityEntry;
      // public selectedPaymentEntry;
    } else {
      this.selectedPriorityID = thisref.priority_mode[1].id;
      this.selectedPriorityEntry = thisref.priority_mode[1];
      this.selectedPayModeID =  thisref.payment_mode[0].id;
      this.selectedPaymentEntry = thisref.payment_mode[0];
    }
  }
}
