import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDosarePensiiAlimentareComponent } from './edit-dosare-pensii-alimentare.component';

describe('EditDosarePensiiAlimentareComponent', () => {
  let component: EditDosarePensiiAlimentareComponent;
  let fixture: ComponentFixture<EditDosarePensiiAlimentareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDosarePensiiAlimentareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDosarePensiiAlimentareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
