import {AfterViewInit, Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-admin-org-user',
  templateUrl: './admin-org-user.component.html',
  styleUrls: ['./admin-org-user.component.css']
})
export class AdminOrgUserComponent implements OnInit, AfterViewInit {
  public selected: string;

  ngAfterViewInit(): void {
  }

  ngOnInit(): void {
  }

  checkValue(item: any) {
    this.selected = item;
  }

  isActive(item: string) {
    return this.selected === item;
  };
}
