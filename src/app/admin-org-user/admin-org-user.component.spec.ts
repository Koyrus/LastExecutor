import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminOrgUserComponent } from './admin-org-user.component';

describe('AdminOrgUserComponent', () => {
  let component: AdminOrgUserComponent;
  let fixture: ComponentFixture<AdminOrgUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminOrgUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminOrgUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
