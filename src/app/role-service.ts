import {Injectable, OnInit} from "@angular/core";
import {Http, RequestOptions, Headers} from "@angular/http";
import {GlobalVariable} from "./global";

@Injectable()
export class RoleService implements OnInit {
  public role: string;
  public baseurl = GlobalVariable.globalApiUrl;

  constructor(private http: Http) {
  }

  ngOnInit(): void {}

  init() {
    // let headers = new Headers();
    // headers.append("Accept", 'text/plain');
    // headers.append('Content-Type', 'application/json');
    // let options = new RequestOptions({ headers: headers });
    // return this.http.post(this.baseurl+'/executor/admin/test' ,options).map(res => res.json()['value']);
  }

  getHeadersOption() {
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    return new RequestOptions({headers: headers});
  }
}
