import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {ConfirmationService} from "primeng/components/common/confirmationservice";
import {Message} from "primeng/components/common/message";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {SelectItem} from "primeng/primeng";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-dosare-mele',
  templateUrl: './dosare-mele.component.html',
  styleUrls: ['./dosare-mele.component.css']
})
export class DosareMeleComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    jQuery('.ui-state-active').css({
      'border-color': 'white!important',
      'background-color': 'white!important',
      'color': 'black!important'
    })
  }

  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa: any[] = [];
  msgs: Message[] = [];
  public case_state: any[] = [];
  displayDialog: boolean;
  brands: SelectItem[];

  constructor(private http: Http, private router: Router, private confirmationService: ConfirmationService, private roleService: RoleService) {
    let thisref = this;
    this.usersPa = [];
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    this.http.post(this.baseApiUrl + "/executor/case/all/data", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.usersPa = res.json();
    });

  }

  detaliiDosar(id: number) {
    this.router.navigateByUrl('/dash/detalii-dosar/' + id)
  }

  editDosar(data: string, id: number) {


    switch (data) {
      case 'PENSIE': {
        this.router.navigateByUrl('/dash/editPensiiAlimentare/' + id);
        break;
      }
      case 'CIVIL': {
        this.router.navigateByUrl('/dash/dosare-civileCreate/' + id);
        break;
      }
      case 'STAT': {
        this.router.navigateByUrl('/dash/DosareStatCreate/' + id);
        break;
      }
    }

  }

  deleteDosar(dosar) {
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };

        this.http.post(this.baseApiUrl + "/executor/case/delete", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {

        });
        let index = this.usersPa.indexOf(dosar);
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  ngOnInit() {
    this.brands = [];
    this.brands.push({label: 'Toate', value: null});
    this.brands.push({label: 'INTENTAT', value: 'INTENTAT'});
    this.brands.push({label: 'SUSPENDAT', value: 'SUSPENDAT'});
    this.brands.push({label: 'STRAMUTAT', value: 'STRAMUTAT'});
    this.brands.push({label: 'EXECUTAT', value: 'EXECUTAT'});
    this.brands.push({label: 'INCHEIAT', value: 'INCHEIAT'});
  }

}
