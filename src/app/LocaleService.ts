import {Injectable} from "@angular/core";
import {IMyLocales} from "mydatepicker/dist";
import {IMyOptions} from "mydatepicker";
import {CurrentDateOptions} from "fullcalendar";

@Injectable()

export class LocaleServiceDate {
  private locales: IMyLocales = {
    "ro": {
      dayLabels: {su: "du", mo: "lu", tu: "ma", we: "mi", th: "jo", fr: "vi", sa: "sa"},
      monthLabels: { 1: "ian", 2: "feb", 3: "mart", 4: "apr", 5: "mai", 6: "iun", 7: "iul", 8: "aug", 9: "sept", 10: "oct", 11: "nov", 12: "dec" },
      dateFormat: "dd.mm.yyyy",
      todayBtnTxt: "Astăzi",
      firstDayOfWeek: "mo",
      sunHighlight: true,
    }
  }



  getLocaleOptions(): IMyOptions {
    // this.es = {
    //   firstDayOfWeek: 1,
    //   dayNames: [ "duminica","luni","marх","miércoles","jueves","viernes","sábado" ],
    //   dayNamesShort: [ "du","lu","ma","mi","jo","vi","sa" ],
    //   dayNamesMin: [ "D","L","M","X","J","V","S" ],
    //   monthNames: [ "enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
    //   monthNamesShort: [ "ene","feb","mar","abr","may","jun","jul","ago","sep","oct","nov","dic" ],
    //   today: 'Hoy',
    //   clear: 'Borrar'
    // }
    return this.locales["ro"];

  }
}
