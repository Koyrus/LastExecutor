import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {Http, RequestOptions ,Headers} from "@angular/http";
import {ConfirmationService, Message, SelectItem} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-dosare-stat',
  templateUrl: './dosare-stat.component.html',
  styleUrls: ['./dosare-stat.component.css']
})

export class DosareStatComponent implements OnInit{
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa : any[]=[];
  public userrs : any[]=[];
  public id_dosar : string = 'new';
  public pensiiAlimentare : any[];
  msgs: Message[] = [];
  displayDialog: boolean;
  constructor(private http : Http , private router : Router,private confirmationService: ConfirmationService ) {

  }
  brands: SelectItem[];
  public stateCase : SelectItem[];
  ngOnInit(): void {
    jQuery('.ui-dropdown-items').css('z-index','-1');
    this.brands = [];
    this.brands.push({label: 'Toate', value: null});
    this.brands.push({label: 'INTENTAT', value: 'INTENTAT'});
    this.brands.push({label: 'SUSPENDAT', value: 'SUSPENDAT'});
    this.brands.push({label: 'STRAMUTAT', value: 'STRAMUTAT'});
    this.brands.push({label: 'EXECUTAT', value: 'EXECUTAT'});
    this.brands.push({label: 'INCHEIAT', value: 'INCHEIAT'});





    console.log('123')

    var thisref = this;
    this.usersPa=[];
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };

    this.http.post(this.baseApiUrl+"/executor/stat/case/all" , postParams , options).subscribe(function (res) {
      thisref.usersPa=res.json();
      for(let i =1;i < thisref.usersPa.length;i++){
        thisref.stateCase = res.json()[i]['case_state']

      }
    });
  }

  deleteDosar(dosar){
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json' );
        let options = new RequestOptions({ headers: headers });

        this.http.post(this.baseApiUrl+"/executor/case/delete", postParams , options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        let index = this.usersPa.indexOf(dosar);
        console.log(index, "index");
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  detaliiDosar(id : number){
    this.router.navigateByUrl('/dash/detalii-dosar/'+id)
  }
  createDosarStat(){
    this.router.navigateByUrl('/dash/DosareStatCreate')
  }

  editDosarStat(id : number){
    this.router.navigateByUrl('/dash/DosareStatCreate/'+id);
    console.log(id);
  }
}
