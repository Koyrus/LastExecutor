import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Http} from "@angular/http";
import {RoleService} from "../role-service";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {MessageService} from "primeng/components/common/messageservice";
import {Message} from "primeng/primeng";

@Component({
  selector: 'app-accountsettings',
  templateUrl: './accountsettings.component.html',
  styleUrls: ['./accountsettings.component.css']
})
export class AccountsettingsComponent implements OnInit,AfterViewInit {
  ngAfterViewInit(): void {
    jQuery('.ui-panel .ui-panel-content').css('padding','0');
    jQuery('.ui-panel-content-wrapper').css('text-align', 'center');
  }

  public id_login_user =  GlobalVariable.login_id;
  public username : string = localStorage.getItem('username');
  public mail : string;
  public curent_password : string = '111111111111111111';
  public new_pass : any;
  public confirm_password : any;
  public field_validate : boolean;
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public msgs: Message[] = [];
  public password_len : boolean =false;

  constructor(private http : Http , private roleService : RoleService , private messageService: MessageService) {}

  ngOnInit() {
    let userData = localStorage.getItem('permissions');
    let dash_contact = JSON.parse(JSON.parse(userData)['_body'])['dash_contact'];
    let mailSplitAfterSplit = dash_contact.split(',')[1];
    this.mail = mailSplitAfterSplit.split(':')[1];
  }

  checkLenght(){
    if(this.new_pass.length < 6){
          this.password_len = true;
          console.log(this.new_pass.length)
    } else {
      this.password_len = false;
    }
  }

  checkIfMail(){
     var str = this.mail;
     this.field_validate = str.includes('@');
  }
  changeAccountSettings(){
        let postData = {
          id_login_user : toInt(this.id_login_user),
          username : this.username,
          password : this.curent_password,
          new_pass : this.new_pass,
          confirm_pass : this.confirm_password,
          email : this.mail
        };
        this.http.put(this.baseApiUrl+'/executor/case/update/login' , postData , this.roleService.getHeadersOption()).subscribe((res)=>{
          if(res.status == 204){
            this.msgs = [];
            this.msgs.push({severity:'error', detail:res.statusText});
          } else if(res.status == 200){
            this.msgs = [];
            this.msgs.push({severity:'success', detail:res.statusText});
          }
        })
  }

}
