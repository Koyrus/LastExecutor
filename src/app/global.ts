export class GlobalVariable{

  ///////////////////////Local//////////////////////////////////
  // public static globalApiUrl = 'http://192.168.1.194:8080';
  // public static globalServiceUrl ='http://localhost:4200/#';
  public static login_id = localStorage.getItem('id');


  ////////////////////////Server/////////////////////////////////
  public static globalApiUrl = 'http://executor.cs1.soft-tehnica.com/REST';
  public static globalServiceUrl ='http://executor.cs1.soft-tehnica.com/#';

//////////////////////////Cloud////////////////////////////////
//   public static globalApiUrl = 'https://rpe.justice.gov.md/REST';
//   public static globalServiceUrl ='https://rpe.justice.gov.md/#';

  /////////////////////////Local HTTPS ////////////////////////////////////
  // public static globalApiUrl = 'https://192.168.1.194:8443';
  // public static globalServiceUrl ='http://localhost:4200/#';


}
