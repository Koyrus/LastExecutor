import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {empty} from "rxjs/Observer";
import {Http, RequestOptions, Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {ActivatedRoute, Router} from "@angular/router";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {Message} from "primeng/primeng";
import {IMyDpOptions} from "mydatepicker";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-creteuserperorganization',
  templateUrl: './creteuserperorganization.component.html',
  styleUrls: ['./creteuserperorganization.component.css']
})
export class CreteuserperorganizationComponent implements OnInit,AfterViewInit {
  ngAfterViewInit(): void {
    jQuery('.ui-panel-titlebar').css('background-color', '#016185');
    jQuery('.ui-panel-titlebar').css('color', 'white');
  }

  public baseApiUrl = GlobalVariable.globalApiUrl;

  public usernameUser: string;
  public oldUserName: string;
  public passwordUser: any;
  public userEmail: any;
  public userIdnp: any;
  public userPhone: any;
  public userName: any;
  public userNameLast: any;
  public roleUser: any[] = [];
  public userFunctions: any[] = [];
  public msgs: Message[] = [];
  public userRoles: any[] = [];
  public userWorkFunctions: any[] = [];
  public id_organization: number;
  public id_user: number;
  public autocomplete_array: any[] = [];
  public global_id: number;
  public streetUser: string;
  public cityUser: string;
  public blocUser: any;
  public bitrhDateUser: any;
  public is_valid: boolean;
  public is_valid_username: boolean;
  public resultss: any[] = [];
  public block_idnp: boolean;
  public disable_inputs: boolean = false;
  public id_org_admin : number;
  public roleuser : any;
  public localDataOrgUser : any[]=[];
  constructor(private formBuilder: FormBuilder, private http: Http, private route: ActivatedRoute, private router: Router, private roleService: RoleService) {
  }


  public myDatePickerOptionsEmitDate: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  };


  onBlurMethod() {

    if (this.id_user != null && this.oldUserName != this.usernameUser) {
      this.validateUserName();

    } else if (this.id_user == null && this.usernameUser != '') {
      this.validateUserName();
    }


  }


  validateUserName() {
    let thisref = this;
    let postParams = {
      id_login_user: -1,
      username: thisref.usernameUser
    };
    this.http.post(this.baseApiUrl + '/executor/admin/username/check', postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.resultss = res.json();
      if (res.json()['user_exist'] == true) {
        thisref.msgs = [];
        thisref.msgs.push({severity: 'error', summary: 'Nume utilizator există'});
        thisref.is_valid_username = true;

      } else {
        thisref.msgs = [];
        thisref.msgs.push({severity: 'success', summary: 'Nume utilizator disponibil'});
        thisref.is_valid_username = false;
      }
    });
  }

  public id_adminOrg : any;
  ngOnInit() {
    var thisref = this;
    this.localDataOrgUser = JSON.parse(JSON.parse(localStorage.getItem('permissions'))['_body'])
    let organization_id = this.localDataOrgUser['id_organisation'];
    console.log(this.localDataOrgUser);
    this.route.params.subscribe(function (params) {
      thisref.id_user = params['id'];
      thisref.id_org_admin = params['org'];
      console.log(thisref.id_user , thisref.id_org_admin,'123')
    });

    if (thisref.id_user > 0) {
      let postParams = {
        id_login_user: toInt(GlobalVariable.login_id),
        id_user: toInt(thisref.id_user),
        id_organization : toInt(thisref.id_org_admin)
      }
      this.http.post(this.baseApiUrl + '/executor/admin/users/create/edit', postParams, this.roleService.getHeadersOption()).subscribe(function (res) {

        thisref.usernameUser = res.json()['username'];
        thisref.oldUserName = res.json()['username'];
        thisref.passwordUser = res.json()['password'];
        thisref.userEmail = res.json()['email'];
        thisref.userPhone = res.json()['phone'];
        thisref.userIdnp = res.json()['idnp'];
        thisref.userName = res.json()['firstname'];
        thisref.userNameLast = res.json()['lastname'];
        thisref.userWorkFunctions = res.json()['work_functions'];
        thisref.userRoles = res.json()['roles'];
        thisref.roleuser = res.json()['role']
        thisref.bitrhDateUser = res.json()['birth_date'];
        thisref.block_idnp = true;
      })
    } else {
      this.id_adminOrg = organization_id;
      let postParamsNew = {
        id_login_user: toInt(GlobalVariable.login_id),
        id_user: null,
        id_organization : toInt(organization_id)
      }
      this.http.post(this.baseApiUrl + '/executor/admin/users/create/edit', postParamsNew, this.roleService.getHeadersOption()).subscribe(function (res) {
        thisref.userWorkFunctions = res.json()['work_functions'];
        thisref.userRoles = res.json()['roles'];
      })
    }
  }

  back() {
    window.history.go(-1);
  }


  idnpAutoComplete() {
    var thisref = this;
    if (this.userIdnp.length == 13) {
      setTimeout(() => {
        let postData = {
          idnp: toInt(this.userIdnp),
          id_login_user: -1,
        };
        this.http.post(this.baseApiUrl + '/executor/case/idnp/search', postData, this.roleService.getHeadersOption()).subscribe(function (res) {
          thisref.autocomplete_array = res.json();
          if (thisref.autocomplete_array['id'] != null && thisref.autocomplete_array['id'] != '') {
            thisref.is_valid = false;
            thisref.global_id = thisref.autocomplete_array['id'];
            thisref.userName = thisref.autocomplete_array['first_name'];
            thisref.userNameLast = thisref.autocomplete_array['last_name'];
          } else {
            thisref.is_valid = true;
          }
        })
      }, 1000);
    } else {
      thisref.disable_inputs = false;
      thisref.is_valid = false;
      thisref.userName = '';
      thisref.userNameLast = '';
    }
  }


  userCreateSave() {
    var thisref = this;
    let postParams = {
      id_login_user: -1,
      role: this.roleUser,
      id_user:toInt(thisref.id_user) > 0 ? thisref.id_user : null,
      email: this.userEmail,
      firstname: this.userName,
      phone: this.userPhone,
      idnp: this.userIdnp,
      is_active: true,
      lastname: this.userNameLast,
      work_function: thisref.userFunctions,
      password: this.passwordUser,
      username: this.usernameUser,
      id_org : toInt(this.id_adminOrg),
      id_person: thisref.global_id,
      birth_date: thisref.bitrhDateUser
    };
    this.http.post(this.baseApiUrl + "/executor/admin/users/save", postParams, this.roleService.getHeadersOption())
      .subscribe(res => {
      })
  }
}
