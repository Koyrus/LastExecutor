import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreteuserperorganizationComponent } from './creteuserperorganization.component';

describe('CreteuserperorganizationComponent', () => {
  let component: CreteuserperorganizationComponent;
  let fixture: ComponentFixture<CreteuserperorganizationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreteuserperorganizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreteuserperorganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
