import { Component, OnInit } from '@angular/core';
import {RoleService} from "../role-service";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {GlobalVariable} from "../global";
import {Http, RequestOptions , Headers} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {ConfirmationService} from "primeng/components/common/confirmationservice";

@Component({
  selector: 'app-registru',
  templateUrl: './registru.component.html',
  styleUrls: ['./registru.component.css']
})
export class RegistruComponent implements OnInit {
  public ro : any;
  public currentDate : any;
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public registerData : any[]=[];
  public date_to : Date;
  public date_from : Date;
  public debitor : string;
  public creditor : string;

  constructor(private headerService : RoleService , private http : Http ,private confirmationService: ConfirmationService) { }

  ngOnInit() {
    this.currentDate = new Date().getFullYear();
    this.maxDateDosar = new Date();
    console.log('init')
    this.ro = {
      firstDayOfWeek: 1,
      dayNames: [ "duminica","luni","marți","miercuri","joi","vineri","sîmbăta" ],
      dayNamesShort: [ "du","lu","ma","mi","jo","vi","sa" ],
      dayNamesMin: [ "D","L","M","M","J","V","S" ],
      monthNames: [ "ianuarie","februarie","martie","aprilie","mai","iunie","iulie","august","septembrie","octombrie","noiembrie","decembrie" ],
      monthNamesShort: [ "ian","feb","mart","apr","mai","iun","iul","aug","sept","oct","nov","dec" ],
      today: 'Astăzi',
      clear: 'Borrar'
    }
  }


  generatePdf() : Observable<any>{
    var thisref = this;



    let headers = new Headers();
     headers.append("Accept", 'plain/text');
     headers.append('Content-Type', 'application/json');
     let options = new RequestOptions({ headers: headers });


    let postData = {
      id_login_user :  toInt(GlobalVariable.login_id),
      date_to : this.date_to != null ? new Date(this.date_to) : null,
      date_from : this.date_from != null ? new Date(this.date_from) : null,
      debitor : this.debitor != null ? this.debitor : null,
      creditor : this.creditor  != null ? this.creditor : null,
    };

    return this.http.post(this.baseApiUrl+'/executor/register/generate/pdf' , postData , options).map(res =>{
      return res['_body'];
    })

    // this.http.post(this.baseApiUrl+'/executor/register/generate/pdf' , postData , options).subscribe((res)=>{
    //   console.log(res['_body']);
    //
    //   var a = document.createElement('a');
    //   a.href=this.baseApiUrl +'/executor/register/download/'+res['_body'];
    //   a.click();
    //
    //
    //
    // })
    // console.log(postData,'postdata')

    // var a = document.createElement('a');
    // a.href=this.baseApiUrl +'/executor/case/download/'+this.date_to+'/'+this.date_from+'/'+this.debitor+'/'+this.creditor;
    // a.click();

  }


  display: boolean = false;
  public registerObject : any[]=[];
  public caseId : number;
  public mentions : string;
  showDialog(con : any[]) {
    this.registerObject = con;

    this.display = true;
    console.log(con)
    this.caseId = con['idCase'];
    this.mentions = con['mentions'];
  };

  saveMention(){
    var thisref = this;
    let post_data = {
      id_login_user : toInt(GlobalVariable.login_id),
      mention : this.mentions,
      id_case : toInt(this.caseId)
    }
    console.log(post_data)
    this.http.post(this.baseApiUrl+'/executor/register/mention/save' , post_data , this.headerService.getHeadersOption()).subscribe((res)=>{
      console.log(res);
      thisref.registerData[this.findRegisterIndex()].mentions = this.mentions;
    })
    this.display = false;

  }


  findRegisterIndex(): number {
    return this.registerData.indexOf(this.registerObject);
  }

  downloadPdf(){
    let nameDoc = this.generatePdf().subscribe((response)=>{
      console.log(response)
      var a = document.createElement('a');
      a.href=this.baseApiUrl +'/executor/register/download/'+response;
      a.click();

    });
  }
  resetTableArray(){
    var thisref = this;
    thisref.registerData = [];
    thisref.date_to = null ;
    thisref.date_from = null ;
    thisref.debitor = null;
    thisref.creditor = null ;
    this.showSaveDocButton = true;

    console.log(thisref.registerData)
  }
  public showSaveDocButton : boolean = true;
  public maxDateDosar : Date;
  loadRegistryData(){
    var thisref = this;
    console.log(this.date_to > new Date());
    let postData = {
      id_login_user :  toInt(GlobalVariable.login_id),
      date_to : this.date_to != null ? new Date(this.date_to) : null,
      date_from : this.date_from != null ? new Date(this.date_from) : null,
      debitor : this.debitor != null ? this.debitor : null,
      creditor : this.creditor  != null ? this.creditor : null,
    };
    console.log(postData);
    this.http.post(this.baseApiUrl+'/executor/register/search' , postData , this.headerService.getHeadersOption()).subscribe((res)=>{
      console.log(res.json());
      thisref.registerData = res.json();
      if(res.json() != [] && res.json().length > 0){
        this.showSaveDocButton = false;
      }
    })
  }

}
