import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistruComponent } from './registru.component';

describe('RegistruComponent', () => {
  let component: RegistruComponent;
  let fixture: ComponentFixture<RegistruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
