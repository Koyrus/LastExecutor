import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public selected :string;

  constructor(private http:Http , private router : Router , private roleService : RoleService) {

  }

  LogOut(){
    localStorage.clear();
    this.router.navigateByUrl('');
  }
  isActive(item:string) {
    return this.selected === item;
  };
  checkValue(item : any){
    var thisref = this;
    this.selected = item;

  }




  typeCategorySpeze(){
    this.router.navigateByUrl('adminDashboard/categorie/speze');
  }

  typeCategoryTaxe(){
    this.router.navigateByUrl('adminDashboard/categorie/taxe');
  }

  typeCategoryAltele(){
    this.router.navigateByUrl('adminDashboard/categorie/altele');
  }

  ngOnInit() {
    var thisref = this;
  }

}
