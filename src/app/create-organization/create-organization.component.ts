import {AfterViewInit, Component, OnInit, Pipe} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global"
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-create-organization',
  templateUrl: './create-organization.component.html',
  styleUrls: ['./create-organization.component.css']
})

export class CreateOrganizationComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {
    jQuery('.ui-panel-titlebar').css('background-color', '#016185');
    jQuery('.ui-panel-titlebar').css('color', 'white');

  }

  public baseApiUrl = GlobalVariable.globalApiUrl;
  public test: any;
  public banksOrganizations: any[] = [];
  public cityOrganizations: any[] = [];
  public street: any = null;
  public block: any = null;
  public bank_account: any;
  public phone: any = null;
  public route_id: number;
  public organizations: any[] = [];
  public selected_bank: any[] = [];
  public selected_city: any[] = [];
  public results: any[] = [];
  public dateCredentials = {
    code: null,
    idno: null,
    email: null,
    city: null,
    addressa: null,
    post_code: null,
    hoursw: null,
    bank: null,
    id: null,
    login_user_id: null,
    name: null,
    phone: null,
    street: null,
    block: null,
    tel_fax: null,
    working_hours: null,
    bank_account: null,
    cod_fiscal: null,
    fiscal_code : null
  };

  constructor(private http: Http, private router: Router, private route: ActivatedRoute, private headerService: RoleService) {
    var thisref = this;

    this.route.params.subscribe(function (params) {
      thisref.route_id = params['id'];
    });
    if (thisref.route_id > 0) {
      let postParams = {
        id_login_user: -1,
        id_organization: toInt(thisref.route_id),
      };
      this.http.post(this.baseApiUrl + '/executor/admin/organization/create/edit', postParams, this.headerService.getHeadersOption()).subscribe(function (res) {
        console.log(res.json());
        thisref.results = res.json();
        thisref.dateCredentials['code'] = thisref.results['licence_code'];
        thisref.selected_bank = thisref.results['bank'];
        thisref.selected_city = thisref.results['city'];
        thisref.dateCredentials['idno'] = thisref.results['idno'];
        thisref.dateCredentials['name'] = thisref.results['name'];
        thisref.dateCredentials['bank_account'] = thisref.results['bank_account'];
        thisref.dateCredentials['email'] = thisref.results['email'];
        thisref.dateCredentials['post_code'] = thisref.results['post_code'];
        thisref.dateCredentials['street'] = thisref.results['location'];
        thisref.dateCredentials['block'] = thisref.results['block'];
        thisref.dateCredentials['tel_fax'] = thisref.results['tel_fax'];
        thisref.dateCredentials['phone'] = thisref.results['phone'];
        thisref.dateCredentials['fiscal_code'] = thisref.results['fiscal_code'];
        thisref.dateCredentials['hoursw'] = thisref.results['working_hours'];
      })
    }
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
  }

  ngOnInit(): void {
    var thisref = this;
    this.http.get(thisref.baseApiUrl + "/executor/admin/nomenclator/org/banks", {}).subscribe(function (res) {
      thisref.banksOrganizations = res.json();
    });
    this.http.get(thisref.baseApiUrl + "/executor/admin/nomenclator/org/city", {}).subscribe(function (res) {
      thisref.cityOrganizations = res.json();
    })
  }

  public sendDate() {
    let postParams = {
      "id": null,
      "id_department": this.route_id,
      "id_login_user": toInt(GlobalVariable.login_id),
      "licence_code": this.dateCredentials.code,
      "name": this.dateCredentials.name,
      "idno": this.dateCredentials.idno,
      "city": this.selected_city,
      "location": this.dateCredentials.street,
      "bank_account": this.dateCredentials.bank_account,
      "bank": this.selected_bank,
      "email": this.dateCredentials.email,
      "post_code": this.dateCredentials.post_code,
      "phone": this.dateCredentials.phone,
      "tel_fax": this.dateCredentials.tel_fax,
      "fiscal_code": this.dateCredentials.fiscal_code,
      "working_hours" : this.dateCredentials.hoursw
    };
    this.http.post(this.baseApiUrl + "/executor/admin/organization/save", postParams, this.headerService.getHeadersOption()).subscribe(res => {
    });
    setTimeout(() => {
      this.router.navigateByUrl("adminDashboard/organization");
    }, 1500);

  }

}
