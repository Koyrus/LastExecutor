import { Component, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {Message} from "primeng/primeng";
import {ActivatedRoute, Router} from "@angular/router";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-other',
  templateUrl: './other.component.html',
  styleUrls: ['./other.component.css']
})
export class OtherComponent implements OnInit {

  public baseApiUrl = GlobalVariable.globalApiUrl;
  public display: boolean = false;
  public msgs: Message[] = [];
  public edit_speze: any[] = [];
  public new_speze: boolean = false;
  public speze_nom: any[] = [];
  public speze_result: any[] = [];

  constructor(private http: Http, private router: Router, private route: ActivatedRoute , private roleService : RoleService) {
  }


  ngOnInit() {
    let thisref = this;
    this.http.get(this.baseApiUrl + '/executor/admin/nompay/category/service/OTHER', this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.speze_result = res.json();
    })
  }


  serviciiSpezeNavigate(id: number) {
    this.router.navigateByUrl('adminDashboard/serviciiSpeze/' + id);
  }

  showDialog(speze) {
    var thisref = this;
    if (speze != null) {
      thisref.edit_speze = speze;
      thisref.new_speze = false;
    } else {
      thisref.edit_speze = [];
      thisref.new_speze = true;
    }
    thisref.display = true;
  }




  dismissModal() {
    let thisref = this;
    thisref.display = false;
  }

  saveSpeze() {
    let thisref = this;

    let PostParams = {
      id_login_user: -1,
      id_category: this.edit_speze['id'],
      type_service: 'OTHER',
      name_service: this.edit_speze['name'],
      is_active: true
    };

    this.http.put(this.baseApiUrl + '/executor/admin/nompay/category/service/save', PostParams, this.roleService.getHeadersOption()).subscribe(res => {
        thisref.speze_nom = res.json();
        if (this.new_speze) {
          thisref.speze_result.push(PostParams);
          thisref.display = false;
          this.msgs = [];
          this.msgs.push({severity: 'success', summary: 'Info Message', detail: 'Successfuly'});
        }
        thisref.display = false;

      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity: 'error', summary: 'Info Message', detail: 'Error'});
        thisref.display = false;

      }
    );
    thisref.display = false;
  }

}
