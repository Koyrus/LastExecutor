import { Component, OnInit } from '@angular/core';
import {GlobalVariable} from "../global";
import {Message} from "primeng/primeng";
import {Http, RequestOptions,Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-taxe',
  templateUrl: './taxe.component.html',
  styleUrls: ['./taxe.component.css']
})
export class TaxeComponent implements OnInit {


  public baseApiUrl = GlobalVariable.globalApiUrl;
  display: boolean = false;
  msgs: Message[] = [];
  public edit_speze : any[]=[];
  public new_speze : boolean = false;
  public categoryName : string;
  constructor(private http : Http,private router : Router,private route : ActivatedRoute) { }


  ngOnInit() {
    var thisref = this;

    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });

    this.http.get(this.baseApiUrl+'/executor/admin/nompay/category/service/TAXE' , options).subscribe(function (res) {
      console.log(res.json());
      thisref.speze_result = res.json();
    })
  }


  serviciiSpezeNavigate(id : number){
    this.router.navigateByUrl('adminDashboard/serviciiSpeze/'+id);
  }

  showDialog(speze) {
    var thisref = this;
    if(speze != null){
      thisref.edit_speze = speze;
      thisref.new_speze = false;
    }else {
      thisref.edit_speze = [];
      thisref.new_speze = true;
    }
    thisref.display = true;
    this.ngOnInit();
  }

  public speze_nom : any[]=[];
  public speze_result : any[]=[];



  dismissModal(){
    var thisref = this;
    thisref.display = false;
    this.ngOnInit();

  }

  saveSpeze(){
    var thisref = this;
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    headers.append('Cache-Control', 'no-cache');

    let options = new RequestOptions({ headers: headers });

    let PostParams = {
      id_login_user:-1,
      id_category : this.edit_speze['id'],
      type_service : 'TAXE',
      name_service : this.edit_speze['name'],
      is_active : true
    }
    console.log(PostParams, 'speze test');

    this.http.put(this.baseApiUrl+'/executor/admin/nompay/category/service/save' , PostParams,options).subscribe(res => {
        console.log(res)
        thisref.speze_nom = res.json();
        if(this.new_speze){
          thisref.speze_result.push(PostParams);
          thisref.display = false;
          console.log(thisref.speze_result,'123123123123123123');
          this.msgs = [];
          this.msgs.push({severity:'success', summary:'Info Message', detail:'Successfuly'});
        }
        thisref.display = false;
        this.ngOnInit();


      }, error => {
        console.log("err");
        this.msgs = [];
        this.msgs.push({severity:'error', summary:'Info Message', detail:'Error'});
        thisref.display = false;

      }
    );
    thisref.display = false;
    this.ngOnInit();

  }

}
