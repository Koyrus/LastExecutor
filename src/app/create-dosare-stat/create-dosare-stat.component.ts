import {Component, OnInit, ViewChild} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {FileUpload, Message} from "primeng/primeng";
import {PersonDto} from "../dosar-civil-create/PersonDto";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {Observable} from "rxjs/Observable";
import {IMyDpOptions} from "mydatepicker";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-create-dosare-stat',
  templateUrl: './create-dosare-stat.component.html',
  styleUrls: ['./create-dosare-stat.component.css']
})
export class CreateDosareStatComponent implements OnInit {
  end_date_case: any;
  @ViewChild("cerere_file") cerere_file: FileUpload;
  public personDtoCase: PersonDto[] = [];
  public choose_file_type: any[] = [];
  public person_debitors: any[] = [];
  public document_type: any;
  public object_type_isactiveGet: boolean = false;
  public type_selected: string = 'FIZIC';
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public index: number = 1;
  public childrens: any[] = [];
  public first_name_debitor: any;
  public last_name_debitor: any;
  public patronimic_debitor: any;
  public case_number: any;
  public created_date: any;
  public state_case: any;
  public idnp_debitor_autocomplete: string = '0';
  public autocomplete_array: any[] = [];
  public priority_mode: any[] = [];
  public emitent_instance: string[] = [];
  public filteredBrands: any[];
  public emitent_resultAutoCompleteCivil: any[] = [];
  public doc_executor: any;
  public selectedPriorityID: number;
  public selectedPriorityEntry;
  public object_execution_list: any[] = [];
  public execution_name: string[] = [];
  public text_procedura: any;
  public amount: any;
  public editDosarStatID: number = null;
  public debitorsArray: any[] = [];
  public case_numberCivil: string;
  public stateTypeDebitor: boolean;
  public case_team: any[] = [];
  public loader: boolean = false;//show loading
  public emitDate: any;
  public testDate: Date;
  public model: any;
  public emit_date: any;
  public amount_eur: number;
  public amount_mdl: number;
  public amount_usd: number;
  public arrayCreditors: any[] = [];
  public dateNow: Date;

  fillData(caseStatEdit: any[], thisref: any) {
    thisref.state_case = caseStatEdit['state_case'];
    thisref.created_PA_date = caseStatEdit['created_date']
    if (caseStatEdit['id'] > 0) {
      thisref.id = caseStatEdit['id'];
      ////First page////
      thisref.uploadedContent = caseStatEdit['files_content'];
      thisref.emitent_instance = caseStatEdit['nom_emitent_instance'];
      thisref.doc_executor = caseStatEdit['doc_executor'];
      thisref.amount = caseStatEdit['amount'];
      thisref.end_date_case = new Date(caseStatEdit['expired_date_case']);
      thisref.text_procedura = caseStatEdit['text_procedure'];
      thisref.execution_name = caseStatEdit['nom_object_execution'];
      thisref.selectedPriorityID = caseStatEdit['nom_priority']['id'];
      thisref.validIDNp = true;
      thisref.selectedPriorityEntry = caseStatEdit['nom_priority'];
      thisref.testDate = new Date(caseStatEdit['case_date']);
      thisref.emit_date = new Date(caseStatEdit['doc_executor_date']);
      thisref.amount_usd = caseStatEdit['amount_usd'];
      thisref.amount_mdl = caseStatEdit['amount_mdl'];
      thisref.amount_eur = caseStatEdit['amount_eur'];




      thisref.object_type_isactiveGet = caseStatEdit['nom_object_execution']['is_active'];

    } else {
      this.selectedPriorityID = thisref.priority_mode[1];
      this.selectedPriorityEntry = thisref.priority_mode[1];
    }
  }

  constructor(private http: Http, private router: Router, private route: ActivatedRoute , private roleService : RoleService) {
    var thisref = this;

    for (let i = 1; i < 10; i++) {
      this.personDtoCase = [];
      this.personDtoCase['index'] = i;
      this.personDtoCase['type_person'] = 'FIZIC';
      this.person_debitors.push(this.personDtoCase);
    }


    this.route.params.subscribe(function (params) {
      thisref.editDosarStatID = params['name'];
    });
    let postParamsCivil = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case_st: null
    };

    if (thisref.editDosarStatID > 0) {
      postParamsCivil.id_case_st = toInt(thisref.editDosarStatID);
    } else {
      postParamsCivil.id_case_st = null;
    }


    this.http.post(this.baseApiUrl + '/executor/stat/case/create/edit', postParamsCivil, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.case_numberCivil = res.json()['case_number'];
      thisref.state_case = res.json()['state_case'];
      thisref.created_date = res.json()['created_date'];
      thisref.case_team = res.json()['case_team'];
      thisref.arrayCreditors = res.json()['debitors'];
      thisref.fillEditPerson(res.json()['debitors'], thisref.person_debitors);

      // for (let i = 0; i < thisref.arrayCreditors.length; i++) {
      //   thisref.type_selected = thisref.arrayCreditors[i]['type_person'];
      // }
      thisref.fillData(res.json(), thisref);
    });
  }

  public test: any[] = [];

  public validIDNp: boolean = false;

  myFuncDebitorIDNP() {
    let thisref = this;
    let debitorData = thisref.person_debitors[this.index - 1];
    let idnp_dat = debitorData.idnp;
    thisref.idnp_debitor_autocomplete = idnp_dat.length;
    if (thisref.person_debitors[thisref.index - 1].idnp.length == 13) {
      this.validIDNp = true;
      setTimeout(() => {
        let postData = {
          idnp: toInt(idnp_dat),
          id_login_user: toInt(GlobalVariable.login_id),
        };

        this.http.post(this.baseApiUrl + '/executor/case/idnp/search', postData, this.roleService.getHeadersOption()).subscribe(function (res) {
          thisref.autocomplete_array = res.json();
          if (thisref.autocomplete_array['id'] != null && thisref.autocomplete_array['id'] != '') {
            thisref.autocomplete_array = res.json();
            debitorData['last_name'] = thisref.autocomplete_array['last_name'];
            debitorData['idnp'] = thisref.autocomplete_array['idnp'];
            debitorData['first_name'] = thisref.autocomplete_array['first_name'];
            debitorData['birth_date'] = new Date(thisref.autocomplete_array['birth_date']);
            debitorData['id'] = thisref.autocomplete_array['id'];
            debitorData['patronimic'] = thisref.autocomplete_array['patronimic'];
            debitorData['post_code'] = thisref.autocomplete_array['post_code'];
            debitorData['street'] = thisref.autocomplete_array['street'];
            debitorData['block'] = thisref.autocomplete_array['block'];
            debitorData['name_org'] = thisref.autocomplete_array['name_org'];
            debitorData['type_person'] = thisref.autocomplete_array['type_person'];
            debitorData['city'] = 'Chișinău';
            debitorData['home_address'] = thisref.autocomplete_array['home_address'];
            debitorData['phone'] = thisref.autocomplete_array['phone'];
            thisref.type_selected = thisref.autocomplete_array['type_person'];
          } else {
            debitorData['id'] = null;
            debitorData['patronimic']= null;

            debitorData['post_code'] = null;
            debitorData['street'] = null;
            debitorData['block'] = null;
            debitorData['name_org'] = null;
            // debitorData['city'] = thisref.autocomplete_array['city'];
            debitorData['city'] = 'Chișinău';
            debitorData['home_address']= null;
            debitorData['phone']= null;
            thisref.stateTypeDebitor = false;
          }
        })
      }, 1000);
    } else {
      if (this.validIDNp) {
        this.clearAllfields(this.index, this.person_debitors);
        this.validIDNp = false
      }
    }
  }

  // public disable_buttonNext : boolean;
  //
  // checkIfIdnpUnique(){
  //   let thisref = this;
  //   let arr = thisref.idnp_array;
  //   let sorted_arr = arr.sort();
  //   let results = [];
  //   for (let i = 0; i < arr.length - 1; i++) {
  //     if (sorted_arr[i + 1] == sorted_arr[i]) {
  //       results.push(sorted_arr[i]);
  //       console.log('already exist');
  //       this.disable_buttonNext = false;
  //     } else console.log('ok!');
  //     this.disable_buttonNext = true;
  //
  //   }
  // }


  ngAfterViewInit(): void {
    jQuery('.card-footer').css('background-color', '#4a89dc');
    jQuery('.card-footer').css('text-align', 'right');
    jQuery('.card-header').css({'font-size': 'medium'})

    jQuery('.btn .btn-secondary .float-right').css('background-color', 'red');
    let buttonPrev = jQuery('.card-footer button:first').text('Precedent');
    let buttonNext = jQuery('.card-footer button:first').next().text('Următorul');
    let buttonPrev2 = jQuery('.card-footer button:last').text('Salvează');
    jQuery('.ui-inputtext').css({
      'border': 'solid 1px',
      'border-color': '#999999',
      'padding-left': '5px',
      'height': '2rem',
      'width': '100%'
    });
    jQuery('.mydp').css({'width': '60%'})

  }


  public myDatePickerOptionsEmitDate: IMyDpOptions = {
    dateFormat: 'dd/mm/yyyy',
  };


  changeTypeDebitor(type_selected, index: number) {
    if (type_selected == 'FIZIC') {
      this.person_debitors[index - 1].type_person = 'FIZIC';
    } else {
      this.person_debitors[index - 1].type_person = 'JURIDIC';
    }
  }


  change_executionType(isAmount: boolean) {
    this.object_type_isactiveGet = isAmount;
  }

  public ro : any;
  public maxDateDosar : Date;
  ngOnInit() {
    let thisref = this;
    this.maxDateDosar = new Date();

    thisref.dateNow = new Date();
    thisref.testDate = new Date();
    console.log(thisref.dateNow);
    this.ro = {
      firstDayOfWeek: 1,
      dayNames: [ "duminica","luni","marți","miercuri","joi","vineri","sîmbăta" ],
      dayNamesShort: [ "du","lu","ma","mi","jo","vi","sa" ],
      dayNamesMin: [ "D","L","M","M","J","V","S" ],
      monthNames: [ "ianuarie","februarie","martie","aprilie","mai","iunie","iulie","august","septembrie","octombrie","noiembrie","decembrie" ],
      monthNamesShort: [ "ian","feb","mart","apr","mai","iun","iul","aug","sept","oct","nov","dec" ],
      today: 'Astăzi',
      clear: 'Borrar'
    }
    this.http.get(this.baseApiUrl + '/executor/case/nomenclator/doctype').subscribe(function (res) {
      thisref.document_type = res.json();
      thisref.choose_file_type = thisref.document_type[0];
    });
    this.http.get(this.baseApiUrl + '/executor/case/nomenclator/priority').subscribe(function (res) {
      thisref.priority_mode = res.json()
    });

    this.http.get(this.baseApiUrl + '/executor/case/nomenclator/object/execution/stat').subscribe(function (res) {
      thisref.object_execution_list = res.json();
    })

  }

  choosePersonDebitors(index: number) {
    let personChoosed = this.person_debitors[index-1];
    this.type_selected = personChoosed.type_person;
    if (personChoosed.id == null) {
    }

  }

  public showDialogIfInstNotFound : boolean;
  msgsInst : Message[];
  filterInst(event) {
    this.filteredBrands = null;
    var thisref = this;
    if (this.emitent_instance.length > 1) {
      this.http.get(this.baseApiUrl + '/executor/case/instance/search/' + this.emitent_instance)
        .subscribe(function (res) {
          thisref.emitent_resultAutoCompleteCivil = res.json();
          if(res.json() == [] || res.json() == ''){
            thisref.emitent_instance = [];
            thisref.showDialogIfInstNotFound = true;
            thisref.msgsInst = [];
            thisref.msgsInst.push({severity:'error', detail:'Instituția Emitentă nu a fost găsită'});
          }
        });
    }
  }

  onSelectionChangePriority(priority) {
    this.selectedPriorityEntry = priority;
    this.selectedPriorityID = priority.id;

  }

//////////////Больше костылей Богу Костылей!!!////////////////
  selectedTeamEntry;
  selectedTeamID: number = null;
  public array_team_members: any[] = [];
  public unique: any[] = [];

  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }

  changeTeamMembers(team) {
    this.selectedTeamEntry = team;
    this.selectedTeamID = team.id;
    this.selectedTeamEntry['is_selected'] = true;
    this.array_team_members.push(this.selectedTeamEntry);
    for (let i = 0; i < this.array_team_members.length; i++) {
      var a = this.array_team_members;
      this.unique = a.filter(CreateDosareStatComponent.onlyUnique);
    }
  }

/////////////////Конец Костыля//////////////////////////////

  public uploadedContent: any[] = [];
  public disable_file: boolean = true;
  public uploadCaseFiles: File[] = [];

  onSelectFile() {
    let tempPostTypeDocs = {
      'id_document': null,
      'file_name': this.cerere_file.files[0].name,
      'doc_type': this.choose_file_type['name'],
      'id_doc_type': this.choose_file_type['id'],
      'is_delete': false
    };
    this.uploadedContent.push(tempPostTypeDocs);
    this.uploadCaseFiles.push(this.cerere_file.files[0]);
    this.cerere_file.clear();
  }

  deleteCaseFileFromArray(file_select: any) {

    if (file_select.id_document != null && file_select.id_document != '') {
      for (var y = 0; y < this.uploadedContent.length; y++) {
        let file_elem = this.uploadedContent[y];
        if (file_elem.id_document === file_select.id_document) {
          file_elem.is_delete = true;
        }
      }
    } else {
      this.uploadedContent = this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
    }

  }

  onChangeFunction() {
    if (this.choose_file_type != null) {
      this.disable_file = false;
    }
  }

  finishDosarC() {
    let thisref = this;
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});
    this.debitorsArray = CreateDosareStatComponent.fillPerson(thisref.person_debitors);
    let idcasecivil = thisref.editDosarStatID != null ? toInt(thisref.editDosarStatID) : null;
    let postParams = {
      //First step/////////////
      id: idcasecivil,
      id_login_user: toInt(GlobalVariable.login_id),
      case_number: this.case_numberCivil,
      state_case: this.state_case,
      doc_executor: this.doc_executor,
      nom_emitent_instance: thisref.emitent_instance,
      files_content: this.uploadedContent,
      doc_type: this.choose_file_type,
      ///Debitor step//////////
      debitors: this.debitorsArray,
      /////Achitari step//////
      case_team: this.unique,
      text_procedure: this.text_procedura,
      nom_object_execution: this.execution_name,
      amount: parseFloat(this.amount),
      nom_priority: this.selectedPriorityEntry,
      expired_date_case: this.end_date_case != null ? this.end_date_case : null,
      case_date: this.testDate,
      doc_executor_date: thisref.emit_date,
      amount_eur: this.amount_eur != null ? this.amount_eur : null,
      amount_mdl: this.amount_mdl != null ? this.amount_mdl : null,
      amount_usd: this.amount_usd != null ? this.amount_usd : null,
    };
    console.log(postParams);
    this.sendDatePAcase(postParams).subscribe(() => {
      this.loader = false;
      this.router.navigateByUrl("dash/dosareStat");
    });

  }


  sendDatePAcase(stepsData: any): Observable<any> {
    let headers = new Headers();
    let form = new FormData();
    for (let i = 0; i < this.uploadCaseFiles.length; i++) {
      form.append("file", this.uploadCaseFiles[i]);
    }
    form.append('case_st', new Blob([JSON.stringify(stepsData)], {
      type: "application/json"
    }));
    this.loader = true;
    return this.http.post(this.baseApiUrl + '/executor/stat/case/save', form, {headers: headers})
      .map(result => {
        return result;
      })
  };

  protected static fillPerson(arrConcat: any[]) {
    let array: any[] = [];
    for (let i = 0; i < arrConcat.length; i++) {
      if (arrConcat[i]['idnp'] != null) {
        var textTest = {
          id: arrConcat[i]['id'],
          birth_date: new Date(arrConcat[i]['birth_date']),
          block: arrConcat[i]['block'],
          city: arrConcat[i]['city'],
          first_name: arrConcat[i]['first_name'],
          home_address: arrConcat[i]['home_address'],
          id_city: 1,
          idnp: arrConcat[i]['idnp'],
          last_name: arrConcat[i]['last_name'],
          name_org: arrConcat[i]['name_org'],
          patronimic: arrConcat[i]['patronimic'],
          phone: arrConcat[i]['phone'],
          post_code: arrConcat[i]['post_code'],
          street: arrConcat[i]['street'],
          type_person: arrConcat[i]['type_person']
        };
        array.push(textTest);
      }
    }
    return array;
  }


  fillEditPerson(arrCaseEdit: any[], person_types: any[]) {
    for (let i = 0; i < person_types.length; i++) {
      let thisref = this;
      if (thisref.editDosarStatID > 0) {
        if (arrCaseEdit[i] !== undefined) {
          person_types[i]['id'] = arrCaseEdit[i]['id'];
          person_types[i]['birth_date'] = new Date(arrCaseEdit[i]['birth_date']);
          person_types[i]['block'] = arrCaseEdit[i]['block'];
          person_types[i]['city'] = arrCaseEdit[i]['city'];
          person_types[i]['first_name'] = arrCaseEdit[i]['first_name'];
          person_types[i]['home_address'] = arrCaseEdit[i]['home_address'];
          person_types[i]['idnp'] = arrCaseEdit[i]['idnp'];
          person_types[i]['last_name'] = arrCaseEdit[i]['last_name'];
          person_types[i]['name_org'] = arrCaseEdit[i]['name_org'];
          person_types[i]['patronimic'] = arrCaseEdit[i]['patronimic'];
          person_types[i]['phone'] = arrCaseEdit[i]['phone'];
          person_types[i]['post_code'] = arrCaseEdit[i]['post_code'];
          person_types[i]['street'] = arrCaseEdit[i]['street'];
          person_types[i]['type_person'] = arrCaseEdit[i]['type_person'];
        }
      }
    }
  }

  clearAllfields(index: number, personsCase: any[]) {
    personsCase[this.index - 1]['first_name'] = null;
    personsCase[this.index - 1]['birth_date'] = null;
    personsCase[this.index - 1]['block'] = null;
    personsCase[this.index - 1]['city'] = null;
    personsCase[this.index - 1]['first_name'] = null;
    personsCase[this.index - 1]['home_address'] = null;
    personsCase[this.index - 1]['last_name'] = null;
    personsCase[this.index - 1]['name_org'] = null;
    personsCase[this.index - 1]['patronimic'] = null;
    personsCase[this.index - 1]['phone'] = null;
    personsCase[this.index - 1]['post_code'] = null;
    personsCase[this.index - 1]['street'] = null;
  }

}
