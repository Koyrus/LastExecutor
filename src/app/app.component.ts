import {AfterViewInit, Component, NgModule, OnInit} from '@angular/core';
import {BrowserModule} from "@angular/platform-browser";
import {DosareStatService} from "./dosare-stat/dosare-stat.service";
import {GlobalVariable} from "./global";
import {PermissionService} from "./permission-service";
import {RoleService} from "./role-service";
import {LocaleServiceDate} from "./LocaleService";
import {IMyDpOptions} from "mydatepicker";
import {MessageService} from "primeng/components/common/messageservice";
import {DatePipe} from "@angular/common";
import {SessionService} from "./login/sessionService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [DosareStatService, PermissionService, RoleService , LocaleServiceDate , MessageService , DatePipe , SessionService]
})
@NgModule({
  imports: [BrowserModule]
})
export class AppComponent implements OnInit, AfterViewInit {

  ngAfterViewInit(): void {
    sessionStorage.setItem('id', GlobalVariable.login_id);
  }

  public baseApiUrl = GlobalVariable.globalApiUrl;

  constructor() {

  }

  ngOnInit() {
  }


}

