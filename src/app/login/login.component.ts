import {Component, NgModule, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {GlobalVariable} from "../global";
import {Http, RequestOptions, Headers} from "@angular/http";
import {Message} from "primeng/primeng";
import {RoleService} from "../role-service";
import {SessionService} from "./sessionService";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@NgModule({
  imports: [BrowserModule]
})
export class LoginComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public username: any;
  public errorMessage: any;
  public password: any;
  public url_path: string = '';
  public msgs: Message[] = [];
  public messageDialogSession : boolean = false;
  constructor(private router: Router, private http: Http, private roleService: RoleService , private dataSession : SessionService) {

  }

  ngOnInit() {
    var thisref = this;
    if(thisref.dataSession.sessionTimer == 0){
      thisref.messageDialogSession = true;
      console.log("expired")
    }
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.Login();
    }
  }

  Login() {
    let thisref = this;

    if (this.username != '' && this.password != '') {

      let postParams = {
        username: this.username,
        password: this.password
      };

      thisref.url_path = '';


      this.http.post(this.baseApiUrl + '/executor/login', postParams, this.roleService.getHeadersOption()).subscribe(function (res) {

        if (res.status == 204) {
          thisref.errorMessage = 'Loginul sau parola sunt incorecte';
        } else {
          console.log(res, 'resLogin');
          let roles = res.json()['role'];
          switch (roles) {
            case 'USER':
              thisref.url_path = 'dash/DosareMele';
              break;
            case 'ADMIN':
              thisref.url_path = 'organization-admin/organization-users';
              break;
            case 'POWER':
              thisref.url_path = 'adminDashboard';
              break;
          }

          GlobalVariable.login_id = res.json()['id_login_user'];
          localStorage.setItem('id', GlobalVariable.login_id);
          localStorage.setItem('username' , thisref.username);
          localStorage.setItem('permissions', JSON.stringify(res));

          thisref.dataSession.timerFunction();


          thisref.router.navigateByUrl(thisref.url_path);

        }

      })

    } else {
      thisref.errorMessage = 'Login și parola sunt obligatorie'
    }
  }
}
