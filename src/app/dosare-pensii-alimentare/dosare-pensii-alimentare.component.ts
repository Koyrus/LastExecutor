import {Component, OnInit} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {Router} from "@angular/router";
import {GlobalVariable} from "../global"
import {ConfirmationService, Message, SelectItem} from "primeng/primeng";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-dosare-pensii-alimentare',
  templateUrl: './dosare-pensii-alimentare.component.html',
  styleUrls: ['./dosare-pensii-alimentare.component.css']
})

export class DosarePensiiAlimentareComponent implements OnInit {
  public msgs: Message[] = [];
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersPa: any[] = [];
  public id_dosar: string = 'new';
  public display: boolean = false;
  public displayDialog: boolean;
  public brands: SelectItem[];
  public total_records: number = 0;


  constructor(private http: Http, private router: Router, private confirmationService: ConfirmationService, private roleService: RoleService) {
    let thisref = this;
    this.usersPa = [];
    let postParams = {
      id_login_user: toInt(GlobalVariable.login_id),
    };
    this.http.post(this.baseApiUrl + "/executor/pa/case/all", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {
      thisref.usersPa = res.json();
    });
  }

  ngOnInit() {
    this.brands = [];
    this.brands.push({label: 'Toate', value: null});
    this.brands.push({label: 'INTENTAT', value: 'INTENTAT'});
    this.brands.push({label: 'SUSPENDAT', value: 'SUSPENDAT'});
    this.brands.push({label: 'STRAMUTAT', value: 'STRAMUTAT'});
    this.brands.push({label: 'EXECUTAT', value: 'EXECUTAT'});
    this.brands.push({label: 'INCHEIAT', value: 'INCHEIAT'});
  }

  deleteDosar(dosar) {
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: dosar.id_case
        };

        this.http.post(this.baseApiUrl + "/executor/case/delete", postParams, this.roleService.getHeadersOption()).subscribe(function (res) {});
        let index = this.usersPa.indexOf(dosar);
        this.usersPa = this.usersPa.filter((val, i) => i != index);
        this.displayDialog = false;
        this.msgs = [{severity: 'success', summary: 'Confirmarea', detail: 'Succes'}];
      },
      reject: () => {
        // this.msgs = [{severity: 'info', summary: 'Rejected', detail: 'You have rejected'}];
      }
    });
  }

  detaliiDosar(id: number) {
    this.router.navigateByUrl('/dash/detalii-dosar/' + id)
  }

  editSteps(id: number) {
    this.router.navigateByUrl('/dash/editPensiiAlimentare/' + id);
  }

  createOrganization() {
    this.router.navigateByUrl('/dash/editPensiiAlimentare/:' + this.id_dosar);
  }

}
