import {Component, OnInit} from '@angular/core';
import {Http, RequestOptions , Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";

@Component({
  selector: 'app-newrole',
  templateUrl: './newrole.component.html',
  styleUrls: ['./newrole.component.css']
})
export class NewroleComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;

  checkboxValue: boolean = false;

  permissions: any[] = ['wasea', 'jora'];
  users : any[] = [];
  role_name : string;
  profiles : { name: string, checked: boolean }[] = [];
  permissions_case : any[]=[];
  selectedPermissions: any[] = [];
  selectedUsers: any[] = [];
  permissions_user : any[]=[];
  items: { name: string, checked: boolean }[] = [];
  checkedPermission : any[]=[];
  caseId : any;
  allPermissions : any[]=[];



  constructor(private http: Http, private router: Router , private route : ActivatedRoute) {
  }

  // selectAll(checked){
  //   this.selectedPermissions = [];
  //   if(checked===true) {
  //     for (let i = 0; i < this.permissions.length; i++) {
  //       this.selectedPermissions.push(this.permissions[i].id);
  //       console.log(this.selectedPermissions , "Selected")
  //
  //     }
  //   }
  // }
  //
  //
  //
  //
  // selectAllUsers(checked){
  //   this.selectedUsers = [];
  //   if(checked===true) {
  //     for (let i = 0; i < this.users.length; i++) {
  //       this.selectedUsers.push(this.users[i].id);
  //     }
  //   }
  // }
  public results : any[]=[];


  public selected_profile : any;
  public selected_profile_id : any;
  public unique : any[]=[];
  public uniqueUser : any[]=[];
  public uniqueProfile : any[]=[];


  static onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
  }



  public checkedPerm : any[]=[];
  public checkedPermUser : any[]=[];
  public checkedProfileUser : any[]=[];
    changeProfile(item){
    this.selected_profile = item;
    this.selected_profile_id = item.id;
  }

  checkedEntry : any;
  checkedEntryId : any;
  checkedUser : any;
  checkedUserId : any;
  checkedProfile : any;
  checkedProfileId : any;



  changeUserPerm(usr_perm){
    this.checkedUser = usr_perm;
    this.checkedUserId = usr_perm.id;
    this.checkedUser['checked'] = true;

    this.checkedPermUser.push(this.checkedUser);
    for(let i = 0;i <this.checkedPermUser.length;i++ ){
      var b = this.checkedPermUser;
      this.uniqueUser = b.filter( NewroleComponent.onlyUnique );
    }
    // console.log(this.uniqueUser , '123')
    console.log(this.uniqueUser);
  }



  changePermissions(permission){
    this.checkedEntry = permission;
    this.checkedEntryId = permission.id;
    this.checkedEntry['checked'] = true;

    this.checkedPerm.push(this.checkedEntry);
    for(let i = 0;i <this.checkedPerm.length;i++ ){
      var a = this.checkedPerm;
      this.unique = a.filter( NewroleComponent.onlyUnique );
    }
    console.log(this.unique , '123')
  }


  changeProfiles(item){
    this.checkedProfile = item;
    this.checkedProfileId = item.id;
    this.checkedProfile['is_active'] = true;

    this.checkedProfileUser.push(this.checkedProfile);
    for(let i = 0;i <this.checkedProfileUser.length;i++ ){
      var a = this.checkedProfileUser;
      this.uniqueProfile = a.filter( NewroleComponent.onlyUnique );
    }
    console.log(this.uniqueProfile , '123')
  }
  public role_id : number;
  ngOnInit() {
    var thisref = this;


    this.route.params.subscribe(function(params){
      thisref.role_id = params['id'];
      console.log(thisref.role_id);
    });


    if(thisref.role_id > 0){
      let headers = new Headers();
      headers.append("Accept", 'application/json');
      headers.append('Content-Type', 'application/json' );

      let options = new RequestOptions({ headers: headers });
      let roleParams = {
        id_login_user : -1,
        id_role: toInt(thisref.role_id),
      };

      this.http.post(this.baseApiUrl+"/executor/admin/roles/create/edit" , roleParams , options )
        .subscribe(res =>{
         console.log(res.json());
         thisref.role_name = res.json()['role_name'];
         thisref.permissions_case = res.json()['permissions_case'];
          thisref.permissions_user = res.json()['permissions_user'];
          thisref.profiles = res.json()['profiles'];
        });
    } else

    console.log(this.checkboxValue , "Check");
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    let options = new RequestOptions({ headers: headers });
    let postParams = {
      id_login_user : -1,
      id_role: null,
    };
    this.http.post(this.baseApiUrl+"/executor/admin/roles/create/edit" , postParams , options )
      .subscribe(res =>{
        // console.log(res.json());
        thisref.results = res.json();
        thisref.profiles = res.json()['profiles'];
        thisref.permissions_case = res.json()['permissions_case'];
        thisref.permissions_user = res.json()['permissions_user'];
      });

    console.log(this.permissions, "Permissions");
    console.log(thisref.profiles , "Profiles");

  }

  sendRoles(){
    var thisref = this;
    console.log(this.checkboxValue , "Check");
    let headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    let options = new RequestOptions({ headers: headers });

    let postParams = {
      id_login_user : -1,
      id_role: this.role_id,
      role_name : thisref.role_name,
      profiles : thisref.uniqueProfile,
      permissions_case : this.unique,
      permissions_user : this.uniqueUser,
      is_organization : true,

    };
    this.http.post(this.baseApiUrl+"/executor/admin/roles/save" ,postParams , options )
      .subscribe(res =>{
        console.log(postParams , "PostParams");
        console.log(res , "Response") ;
      })
  }
}
