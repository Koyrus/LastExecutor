import { Component, OnInit } from '@angular/core';
import {Http, RequestOptions,Headers} from "@angular/http";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {RoleService} from "../role-service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.css']
})
export class AdminUsersComponent implements OnInit {
  public baseApiUrl = GlobalVariable.globalApiUrl;
  public usersOrganization : any[]=[];
  constructor(private http : Http , private roleService : RoleService,private router : Router) { }

  ngOnInit() {
    let postParams ={
      id_login_user : toInt(GlobalVariable.login_id)
    };
    this.http.post(this.baseApiUrl+'/executor/admin/users/organization',postParams,this.roleService.getHeadersOption()).subscribe(res=>{
      this.usersOrganization = res.json();
    })
  }
  createUser(org : string){
    this.router.navigateByUrl('organization-admin/new-organization-user');

  }
  editUser(id :string , org : string){
    this.router.navigateByUrl('organization-admin/create-editOrganizationUser/'+id+'/'+org);

  }
}
