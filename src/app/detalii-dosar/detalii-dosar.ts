import {AfterViewInit, Component, Injectable, OnInit, ViewChild} from '@angular/core';
import {Http, RequestOptions, Headers} from "@angular/http";
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalVariable} from "../global";
import {toInt} from "ngx-bootstrap/bs-moment/utils/type-checks";
import {ConfirmationService, FileUpload, Message, TabView} from "primeng/primeng";
import {Observable} from "rxjs/Observable";
import {GlobalService} from "../globalService";
import {DosareStatService} from "../dosare-stat/dosare-stat.service";
import {RoleService} from "../role-service";

@Component({
  selector: 'app-detalii-dosar',
  templateUrl: './detalii-dosar.html',
  styleUrls: ['./detalii-dosar.css']
})
@Injectable()
export class DetaliiDosarComponent implements OnInit, AfterViewInit {
  //Public Variables of this class//
  public orderPaymentsArray: any[] = [];
  public error_message: any;
  public display_error: boolean;
  public id_file: number;
  public editDosarID: number = null;
  public main_url = GlobalVariable.globalApiUrl;
  public case_number: any;
  public nom_emitent_instance: string;
  public doc_executor: string;
  public created_date: any;
  public expired_date_case: string;
  public debitor_credentials: any[] = [];
  public creditor_credentials: any[] = [];
  public childrens: any[] = [];
  public files_content: any[] = [];
  public files_pdf: any[] = [];
  public typeCase: string;
  public msgs: Message[] = [];
  public history_case: any[] = [];
  public displayDialog: boolean;
  public stateCase: string;
  private id_msign: number;
  public signedDoc: boolean = false
  public creditor: string;
  public debitor: string;
  public selectedTypeEntry: any;
  public id_person: number;
  public disableTypeCreditor: boolean = true;
  public disableTypeDebitor: boolean = true;
  public disableTypeOther: boolean = true;
  public typeIsEmpty: boolean = true;
  public personOther: any;
  public borderou_creditors_array: any[] = [];
  public borderou_debitors_array: any[] = [];
  public for_pay: any;
  public is_stat: boolean;
  public selected_person_fromArray: any[] = [];
  public displayPaymentModel: boolean = false;
  public uploadedContent: any[] = [];
  public borderou_list: any[] = [];
  public borderou_item_selected: any[] = [];
  public selectedBorderouId: any;
  public displayPaymentProperties: boolean = false;
  public date_payed: any;
  public order_payments: any[] = [];
  public payedAmount: number = 0;
  public messagePayed: string;
  public ro: any;
  public today: any;
  public test: string;
  public selectedFirstBorderou: number;
  public motiv_anularii: string;
  public selectedIndex = 0;
  @ViewChild(TabView) tabView: TabView;
  @ViewChild("cerere_file") cerere_file: FileUpload;

  constructor(private http: Http, private router: Router, private route: ActivatedRoute, private confirmationService: ConfirmationService, private detaliiService: DosareStatService, private headerService: RoleService) {}
 ///Lifecycle functions///
  ngAfterViewInit() {}

  ngOnInit() {
    var thisref = this;
    this.today = new Date();
    jQuery('.card-footer').css({'background-color': '#4a89dc', 'text-align': 'right'});
    jQuery('.ui-fieldset-legend .ui-corner-all .ui-state-default .ui-unselectable-text').css({'background-color': 'cyan'})

    this.ro = {
      firstDayOfWeek: 1,
      dayNames: ["duminica", "luni", "marți", "miercuri", "joi", "vineri", "sîmbăta"],
      dayNamesShort: ["du", "lu", "ma", "mi", "jo", "vi", "sa"],
      dayNamesMin: ["D", "L", "M", "M", "J", "V", "S"],
      monthNames: ["ianuarie", "februarie", "martie", "aprilie", "mai", "iunie", "iulie", "august", "septembrie", "octombrie", "noiembrie", "decembrie"],
      monthNamesShort: ["ian", "feb", "mart", "apr", "mai", "iun", "iul", "aug", "sept", "oct", "nov", "dec"],
      today: 'Astăzi',
      clear: 'Borrar'
    };


    this.route.params.subscribe(function (params) {
      thisref.editDosarID = params['name'];
      thisref.id_msign = params['sign'];

      if (thisref.id_msign) {
        thisref.signedDoc = true;
      }


    });

    this.getDetailsData().subscribe((response) => {

      console.log(response);
      thisref.borderou_list = response['list_borderou'];
      thisref.selectedFirstBorderou = response['first_borderou'];
      thisref.case_number = response['case_number'];
      thisref.doc_executor = response['doc_executor'];
      thisref.nom_emitent_instance = response['emitent_instance'];
      thisref.created_date = response['created_date'];
      thisref.expired_date_case = response['expired_date_case'];
      thisref.debitor_credentials = response['debitors'];
      thisref.creditor_credentials = response['creditors'];
      thisref.childrens = response['children'];
      thisref.typeCase = response['type'];
      thisref.stateCase = response['state_case'];
      thisref.files_pdf = response['files_pdf'];
      thisref.files_content = response['files_content'];
      thisref.detaliiService.getDebitors = thisref.debitor_credentials;
      thisref.detaliiService.getCreditors = thisref.creditor_credentials;
      thisref.borderou_item_selected = thisref.borderou_list[0];
    });


  }
  ///End of Lifecycle functions///

  ///Payment functions///
  paymentSave() {
    var thisref = this;

    if (thisref.payedAmount > thisref.order_payments['totalAmount']) {
      thisref.messagePayed = 'Suma indicată depășește suma totală';
      return;
    } else {

      let postParams = {
        id_login_user: toInt(GlobalVariable.login_id),
        id_order: toInt(this.order_payments['id']),
        date_payed: new Date(this.date_payed),
        amount_payed: toInt(this.payedAmount)
      };

      this.http.post(this.main_url + '/executor/case/payments/order/transaction', postParams, this.getHeadersOption()).subscribe((res) => {
        if (res.status == 200) {
          let index = thisref.orderPaymentsArray.indexOf(thisref.order_payments);
          thisref.order_payments['dueAmount'] += thisref.payedAmount;
          let payed = thisref.order_payments['totalAmount'] - thisref.order_payments['dueAmount'];

          if (payed <= 0) {
            thisref.order_payments['isPayed'] = true
          }
          // this.testPaymentArray.join()
          thisref.messagePayed = '';
          thisref.displayPaymentProperties = false;
          thisref.orderPaymentsArray[index] = thisref.order_payments;

        }
      })
    }
  }

  deleteBill(bill: any[]) {
    var thisref = this;
    let postDataBill = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_order: toInt(bill['id'])
    }
    console.log(postDataBill)
    this.http.post(this.main_url + '/executor/case/payments/order/delete', postDataBill, this.getHeadersOption()).subscribe((res) => {
      if (res.status == 200) {
        let index = thisref.orderPaymentsArray.indexOf(bill);
        this.orderPaymentsArray = this.orderPaymentsArray.filter((val, i) => i != index);

      }
    })
  }

  downdloadBill(bill: any) {
    var a = document.createElement('a');
    a.href = this.main_url + '/executor/case/download/' + bill;
    a.click();
  }

  personTypeBorderou(person_type) {
    var thisref = this;
    this.selectedTypeEntry = person_type;
    if (this.selectedTypeEntry != 'debitor' || this.selectedTypeEntry != 'creditor' || this.selectedTypeEntry != 'other_person') {
      thisref.typeIsEmpty = false;
    }
    if (this.selectedTypeEntry == 'debitor') {
      this.disableTypeCreditor = true;
      this.disableTypeOther = true;
      this.disableTypeDebitor = false;
    } else if (this.selectedTypeEntry == 'creditor') {
      this.disableTypeDebitor = true;
      this.disableTypeOther = true;
      this.disableTypeCreditor = false;
    } else if (this.selectedTypeEntry == 'other_person') {
      this.disableTypeDebitor = true;
      this.disableTypeCreditor = true;
      this.disableTypeOther = false;
      this.selected_person_fromArray = null;
      this.id_person = null;
    }
  }

  showDialogPayment() {
    var thisref = this
    this.displayPaymentModel = true;
    let postBorderouDate = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case: toInt(this.editDosarID),
      id_borderou: toInt(thisref.selectedBorderouId)
    };
    this.http.post(this.main_url + '/executor/case/payments/create/borderou/order', postBorderouDate, this.getHeadersOption()).subscribe((res) => {
      thisref.borderou_creditors_array = res.json()['creditors'];
      thisref.borderou_debitors_array = res.json()['debitors'];
      thisref.for_pay = res.json()['for_pay'];
      thisref.is_stat = res.json()['is_stat'];
    })
  }

  chooseTypeBorderou() {
    var thisref = this;
    console.log(this.borderou_item_selected);
    thisref.selectedBorderouId = this.borderou_item_selected['id'];
    let postDataBorderou = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_borderou: toInt(this.borderou_item_selected['id'])
    }
    this.http.post(this.main_url + '/executor/case/payments/borderou/orders', postDataBorderou, this.getHeadersOption()).subscribe((res) => {
      thisref.orderPaymentsArray = res.json()
    })
  }

  savePaymentOrder() {
    var thisref = this;
    if (this.selected_person_fromArray != null) {
      this.id_person = this.selected_person_fromArray['id'];
      this.personOther = this.selected_person_fromArray['name'];
    }
    let postOrderParams = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_borderou: toInt(thisref.selectedBorderouId),
      id_person: this.id_person,
      person_name: this.personOther
    };

    this.http.post(this.main_url + '/executor/case/orders/save', postOrderParams, this.getHeadersOption()).subscribe((res) => {
      if (res.status == 200) {
        this.displayPaymentModel = false;
        this.orderPaymentsArray.push({
          'id': res.json()['id_order'],
          'personPayer': postOrderParams.person_name,
          'created': new Date().toLocaleDateString('ru-RU'),
          'code': thisref.case_number,
          'totalAmount': thisref.for_pay,
          'attachmentId': res.json()['id_attachment']
        });
      }
    })

  }

  showPropertiesModal(order: any) {
    this.displayPaymentProperties = true;
    this.order_payments = order;
    this.payedAmount = order.dueAmount;
  }

  ///End of Payment functions///

  //## Case functions ##//
  EditFromDetails() {

    switch (this.typeCase) {
      case 'PENSIE': {
        this.router.navigateByUrl('/dash/editPensiiAlimentare/' + this.editDosarID);
        break;
      }
      case 'CIVIL': {
        this.router.navigateByUrl('/dash/dosare-civileCreate/' + this.editDosarID);
        break;
      }
      case 'STAT': {
        this.router.navigateByUrl('/dash/DosareStatCreate/' + this.editDosarID);
        break;
      }
    }
  }

  generateDoc(editDosarID: any) {
    this.router.navigateByUrl('/dash/GenerateDocument/' + editDosarID);
  }

  deleteDosar() {
    var thisref = this;
    this.confirmationService.confirm({
      message: 'Sigur doriți să ștergeți ?',
      header: 'Confirmare',
      icon: 'fa fa-trash',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_case: toInt(thisref.editDosarID)
        };
        var headers = new Headers();
        headers.append("Accept", 'application/json');
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({headers: headers});

        this.http.post(this.main_url + "/executor/case/delete", postParams, options).subscribe(function (res) {
          console.log(res);
          console.log(postParams)

        });
        setTimeout(() => {
          this.router.navigateByUrl("dash/DosareMele");
        }, 2000);
      },
    });
  }

  changeStateCase() {
    var thisref = this;
    let postParamsCase = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case: toInt(thisref.editDosarID),
      select_state: thisref.stateCase
    };

    this.http.post(this.main_url + "/executor/case/state/change", postParamsCase, this.headerService.getHeadersOption()).subscribe(function (res) {
    })

  }

  // ### Tab options ##////

  getDetailsData(): Observable<any> {
    let postParamsDetails = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_case: toInt(this.editDosarID),
      id_sign: this.id_msign != null ? toInt(this.id_msign) : null
    };
    return this.http.post(this.main_url + "/executor/case/details", postParamsDetails, this.getHeadersOption())
      .map(result => {
        return result.json();
      })
  }

  tabClick($event) {
    const SELECTED_PLATI = 3;
    const HISTORY_CASE = 5;
    this.selectedIndex = $event.index;
    console.log(this.tabView.tabs[this.selectedIndex].header);
    console.log(this.selectedIndex, 'selectedIndex');

    switch (this.selectedIndex) {
      case SELECTED_PLATI:
        this.chooseTypeBorderou();
        break;
      case HISTORY_CASE:
        this.historyDetailsCase();
        break;
    }

  }

  historyDetailsCase() {
    var thisref = this;
    let postParams = {
      id_case: toInt(thisref.editDosarID)
    };

    this.http.post(this.main_url + "/executor/case/details/history", postParams, this.getHeadersOption()).subscribe(function (res) {
      if (res.status == 200) {
        thisref.history_case = res.json();
      }
    })
  }


  //## MSign component ##/////
  anulareDocument(id_file: number) {
    var thisref = this;
    this.confirmationService.confirm({
      message: '',
      accept: () => {
        let postParams = {
          id_login_user: toInt(GlobalVariable.login_id),
          id_file: toInt(id_file),
          reason: thisref.motiv_anularii
        };
        this.http.post(this.main_url + "/executor/case/document/msign/cancel", postParams, this.getHeadersOption()).subscribe(function (res) {
          thisref.ngOnInit();
        })
      }
    });
  }

  signDocument(id_file: number, id_doc_type: number) {
    var thisref = this;
    let postParamsSign = {
      id_login_user: toInt(GlobalVariable.login_id),
      id_file: toInt(id_file),
      id_case: toInt(thisref.editDosarID),
      id_doc_type: toInt(id_doc_type)
    };
    thisref.display_error = false;
    this.http.post(this.main_url + "/executor/case/document/msign", postParamsSign, this.getHeadersOption()).subscribe(function (res) {
      let hash = res.json()['hash_key'];
      var form = document.createElement("form");
      form.setAttribute("method", 'GET');
      form.setAttribute("action", 'https://testmsign.gov.md/' + hash);
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", 'ReturnUrl');
      hiddenField.setAttribute("value", GlobalVariable.globalServiceUrl + '/dash/detalii-dosar/' + thisref.editDosarID + '/' + id_file);
      form.appendChild(hiddenField);
      document.body.appendChild(form);
      form.submit();
    }, error => {
      thisref.error_message = error.json()['signed_error'];
      thisref.display_error = true;
    })


  }


  //##### Local headers options (use RoleService.getHearersOptions or this function) ##/////
  getHeadersOption() {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json');
    let options = new RequestOptions({headers: headers});
    return options;
  }

  //###### File upload function (Don't remove it!) #########
  // onSelectFile() {
  //   let tempPostTypeDocs = {
  //     'id_document': null,
  //     'file_name': this.cerere_file.files[0].name,
  //     'doc_type': this.choose_file_type['name'],
  //     'id_doc_type': this.choose_file_type['id'],
  //     'is_delete': false
  //   };
  //   this.uploadedContent.push(tempPostTypeDocs);
  //   this.uploadCaseFiles.push(this.cerere_file.files[0]);
  //   this.cerere_file.clear();
  // }
  //
  // deleteCaseFileFromArray(file_select: any) {
  //   if (file_select.id_document != null && file_select.id_document != '') {
  //     for (var y = 0; y < this.uploadedContent.length; y++) {
  //       let file_elem = this.uploadedContent[y];
  //       if (file_elem.id_document === file_select.id_document) {
  //         file_elem.is_delete = true;
  //       }
  //     }
  //   } else {
  //     this.uploadedContent = this.uploadedContent.filter((val, i) => val.file_name != file_select.file_name && file_select.id_document == null);
  //   }
  // }
  //######//////////////////////////////////
}
